import { SystemMobileMenuPermission, SystemMobileMenuButtonQuery, SystemMobileMenuButtonDelete, SystemMobileMenuButtonIsFreeze } from '@/services/api'
import { request, METHOD } from '@/utils/request'

/**
 * 树
 */
export function menuQuery() {
    return request(SystemMobileMenuPermission, METHOD.GET, {})
}

/**
 * 列表
 */
export function query(param) {
    return request(SystemMobileMenuButtonQuery, METHOD.POST, param)
}

/**
 * 删除
 */
export async function del(param) {
    return request(SystemMobileMenuButtonDelete, METHOD.POST, param)
}
/**
 * 
 */
export function isFreeze(param) {
    return request(SystemMobileMenuButtonIsFreeze, METHOD.POST, param)
}
export default {
    menuQuery,
    query,
    del,
    isFreeze
}