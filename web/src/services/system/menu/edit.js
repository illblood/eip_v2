import { SystemMenu, SystemMenuSave, SystemMenuFindById } from '@/services/api'
import { request, METHOD } from '@/utils/request'
/**
 * 树
 */
export function menuQuery() {
    return request(SystemMenu, METHOD.GET, {})
}
/**
 * 保存
 */
export async function save(form) {
    return request(SystemMenuSave, METHOD.POST, form)
}

/**
 * 根据Id获取
 */
export function findById(id) {
    return request(SystemMenuFindById + "/" + id, METHOD.GET, {})
}
export default {
    save,
    findById,
    menuQuery
}