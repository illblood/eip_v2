import { SystemOrganization, SystemOrganizationQuery, SystemOrganizationDelete, SystemOrganizationIsFreeze } from '@/services/api'
import { request, METHOD } from '@/utils/request'

/**
 * 树
 */
export function organizationQuery() {
    return request(SystemOrganization, METHOD.GET, {})
}

/**
 * 列表
 */
export function query(param) {
    return request(SystemOrganizationQuery, METHOD.POST, param)
}

/**
 * 删除
 */
export async function del(param) {
    return request(SystemOrganizationDelete, METHOD.POST, param)
}
/**
 * 
 */
export function isFreeze(param) {
    return request(SystemOrganizationIsFreeze, METHOD.POST, param)
}
export default {
    organizationQuery,
    query,
    del,
    isFreeze
}