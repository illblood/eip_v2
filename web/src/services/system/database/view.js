import { AgileDataBaseView } from '@/services/api'
import { request, METHOD } from '@/utils/request'

/**
 * 空间
 */
export function view() {
    return request(AgileDataBaseView, METHOD.GET, {})
}

export default {
    view
}