import { AgileConfigQuery, AgileDataBaseTableColumn, AgileConfigSaveJson, AgileConfigPublicJson, AgileConfigFindById, AgileDataBaseSaveFormTableField } from '@/services/api'
import { request, METHOD } from '@/utils/request'

/**
 * 保存
 */
export async function listSave(form) {
    return request(AgileConfigSaveJson, METHOD.POST, form)
}
/**
 * 发布
 */
export async function listPublic(form) {
    return request(AgileConfigPublicJson, METHOD.POST, form)
}
/**
 * 列
 */
export async function column(param) {
    return request(AgileDataBaseTableColumn, METHOD.POST, param)
}
/**
 * 根据Id获取
 */
export async function findById(id) {
    return request(AgileConfigFindById + "/" + id, METHOD.GET, {})
}

/**
 * 列表
 */
export async function query(param) {
    return request(AgileConfigQuery, METHOD.POST, param)
}
export default {
    listSave,
    listPublic,
    findById,
    column,
    query
}