import { AgileConfigSaveJson, AgileConfigPublicJson, AgileConfigFindById, AgileDataBaseSaveFormTableField } from '@/services/api'
import { request, METHOD } from '@/utils/request'

/**
 * 保存
 */
export async function editSave(form) {
    return request(AgileConfigSaveJson, METHOD.POST, form)
}
/**
 * 发布
 */
export async function editPublic(form) {
    return request(AgileConfigPublicJson, METHOD.POST, form)
}

/**
 * 根据Id获取
 */
export async function findById(id) {
    return request(AgileConfigFindById + "/" + id, METHOD.GET, {})
}

/**
 * 修改表字段
 */
export async function tableField(param) {
    return request(AgileDataBaseSaveFormTableField, METHOD.POST, param)
}
export default {
    editSave,
    editPublic,
    findById,
    tableField
}