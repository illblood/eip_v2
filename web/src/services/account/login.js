import { SystemLogin, SystemUserMenu, SystemCaptcha } from '@/services/api'
import { request, METHOD, removeAuthorization } from '@/utils/request'

/**
 * 登录服务
 * @param code 账户名
 * @param password 账户密码
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function login(param) {
    return request(SystemLogin, METHOD.POST, param)
}

/**
 * 菜单
 */
export async function menu() {
    return request(SystemUserMenu, METHOD.GET, {})
}
/**
 * 验证码
 */
export async function captcha() {
    return request(SystemCaptcha, METHOD.GET, {}, { responseType: 'blob' })
}

/**
 * 退出登录
 */
export function logout() {
    localStorage.removeItem(process.env.VUE_APP_ROUTES_KEY)
    localStorage.removeItem(process.env.VUE_APP_PERMISSIONS_KEY)
    localStorage.removeItem(process.env.VUE_APP_ROLES_KEY)
    removeAuthorization()
}
export default {
    captcha,
    login,
    logout,
    menu
}