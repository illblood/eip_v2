import Vue from 'vue'

/**
 * 结果代码
 */
const Resultcode = {
    success: 200,
    error: 500
}
Vue.prototype.eipResultCode = Resultcode;

/**
 * 空Guid
 */
Vue.prototype.eipEmptyGuid = "00000000-0000-0000-0000-000000000000";

const Msg = {
    delete: "删除后不可恢复,确认删除?",
    delloading: "正在删除中...",
    saveloading: '保存中...',
    copyloading: "正在复制保存中...",
    buy: "当前版本未开放,请联系(QQ:1039318332)购买高级版本"
}

Vue.prototype.eipMsg = Msg;

/**
 * 权限类型
 */
const privilegeMaster = {
    role: 0, //角色
    organization: 1, //组织架构
    group: 2, //组
    post: 3, //岗位
    user: 4 //用户
}
Vue.prototype.eipPrivilegeMaster = privilegeMaster;

/**
 * 权限归属
 */
const privilegeAccess = {
    menu: 0, //菜单
    menubutton: 1, //模块按钮
    field: 2, //字段
    data: 3, //数据
    workflow: 4, //数据
    mobilemenu: 5, //模块菜单
    mobilemenubutton: 6 //模块菜单
}
Vue.prototype.eipPrivilegeAccess = privilegeAccess;

/**
 * 分页参数
 */
Vue.prototype.eipPage = {
    size: 50,
    sizeOptions: ["50", "100", "150", "200"]
};

const echarts = require('echarts')
    /**
     * echarts报表
     */
Vue.prototype.eipEcharts = echarts

Vue.prototype.eipEnum = {
    "流程类别": "6f67e506-5476-438e-8ffb-ae680112710d",
    "表单类别": "703e0a56-6d81-4198-a721-ae680112a7ff",
    "移动构建": "c2416d07-b9ce-458a-867e-aef8011a8fbf",
};


/**
 *敏捷开发设计对象
 */
Vue.prototype.eipAgileDesinger = {
    kfd: null
}


/**
 * 生成表前缀
 */
Vue.prototype.eipTablePrefix = "eip_";

/**
 * 获取减去顶部高度
 */
Vue.prototype.eipHeaderHeight = function() {
    var height = window.innerHeight;
    var setting = this.$store.state.setting
    height -= setting.multiPage ? 91 : 50;
    return height;
}

/**
 * 工作流处理类型
 */
Vue.prototype.eipWorkflowDoType = {
    "审核": 1,
    "知会": 2,
    "加签": 3,
    "阅示": 4,
    "流程监控": 5,
    "阅示审批": 6,
};