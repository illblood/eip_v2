﻿using System.ComponentModel.DataAnnotations;

namespace EIP.Common.Pay.Dto
{
    public class WeChatPayMicroPayDto
    {
        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }
        
        [Display(Name = "body")]
        public string Body { get; set; }
        
        [Display(Name = "total_fee")]
        public int TotalFee { get; set; }
        
        [Display(Name = "spbill_create_ip")]
        public string SpBillCreateIp { get; set; }
        
        [Display(Name = "auth_code")]
        public string AuthCode { get; set; }
    }

    public class WeChatPayPubPayDto
    {
        
        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }
        
        [Display(Name = "body")]
        public string Body { get; set; }
        
        [Display(Name = "total_fee")]
        public int TotalFee { get; set; }
        
        [Display(Name = "spbill_create_ip")]
        public string SpBillCreateIp { get; set; }
        
        [Display(Name = "notify_url")]
        public string NotifyUrl { get; set; }
        
        [Display(Name = "trade_type")]
        public string TradeType { get; set; }
        
        [Display(Name = "openid")]
        public string OpenId { get; set; }
    }

    public class WeChatPayQrCodePayDto
    {
        
        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }
        
        [Display(Name = "body")]
        public string Body { get; set; }
        
        [Display(Name = "total_fee")]
        public int TotalFee { get; set; }
        
        [Display(Name = "spbill_create_ip")]
        public string SpBillCreateIp { get; set; }
        
        [Display(Name = "notify_url")]
        public string NotifyUrl { get; set; }
        
        [Display(Name = "trade_type")]
        public string TradeType { get; set; }
    }

    public class WeChatPayAppPayDto
    {
        
        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }
        
        [Display(Name = "body")]
        public string Body { get; set; }
        
        [Display(Name = "total_fee")]
        public int TotalFee { get; set; }
        
        [Display(Name = "spbill_create_ip")]
        public string SpBillCreateIp { get; set; }
        
        [Display(Name = "notify_url")]
        public string NotifyUrl { get; set; }
        
        [Display(Name = "trade_type")]
        public string TradeType { get; set; }
    }

    public class WeChatPayH5PayDto
    {
        
        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }
        
        [Display(Name = "body")]
        public string Body { get; set; }

        
        [Display(Name = "total_fee")]
        public int TotalFee { get; set; }

        
        [Display(Name = "spbill_create_ip")]
        public string SpBillCreateIp { get; set; }

        
        [Display(Name = "notify_url")]
        public string NotifyUrl { get; set; }

        
        [Display(Name = "trade_type")]
        public string TradeType { get; set; }
    }

    public class WeChatPayLiteAppPayDto
    {
        
        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }

        
        [Display(Name = "body")]
        public string Body { get; set; }

        
        [Display(Name = "total_fee")]
        public int TotalFee { get; set; }

        
        [Display(Name = "spbill_create_ip")]
        public string SpBillCreateIp { get; set; }

        
        [Display(Name = "notify_url")]
        public string NotifyUrl { get; set; }

        
        [Display(Name = "trade_type")]
        public string TradeType { get; set; }

        
        [Display(Name = "openid")]
        public string OpenId { get; set; }
    }

    public class WeChatPayOrderQueryDto
    {
        [Display(Name = "transaction_id")]
        public string TransactionId { get; set; }

        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }
    }

    public class WeChatPayReverseDto
    {
        [Display(Name = "transaction_id")]
        public string TransactionId { get; set; }

        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }
    }

    public class WeChatPayCloseOrderDto
    {
        
        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }
    }

    public class WeChatPayRefundDto
    {
        
        [Display(Name = "out_refund_no")]
        public string OutRefundNo { get; set; }

        [Display(Name = "transaction_id")]
        public string TransactionId { get; set; }

        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }

        
        [Display(Name = "total_fee")]
        public int TotalFee { get; set; }

        
        [Display(Name = "refund_fee")]
        public int RefundFee { get; set; }

        [Display(Name = "refund_desc")]
        public string RefundDesc { get; set; }

        [Display(Name = "notify_url")]
        public string NotifyUrl { get; set; }
    }

    public class WeChatPayRefundQueryDto
    {
        [Display(Name = "refund_id")]
        public string RefundId { get; set; }

        [Display(Name = "out_refund_no")]
        public string OutRefundNo { get; set; }

        [Display(Name = "transaction_id")]
        public string TransactionId { get; set; }

        [Display(Name = "out_trade_no")]
        public string OutTradeNo { get; set; }
    }

    public class WeChatPayDownloadBillDto
    {
        
        [Display(Name = "bill_date")]
        public string BillDate { get; set; }

        
        [Display(Name = "bill_type")]
        public string BillType { get; set; }

        [Display(Name = "tar_type")]
        public string TarType { get; set; }
    }

    public class WeChatPayDownloadFundFlowDto
    {
        
        [Display(Name = "bill_date")]
        public string BillDate { get; set; }

        
        [Display(Name = "account_type")]
        public string AccountType { get; set; }

        [Display(Name = "tar_type")]
        public string TarType { get; set; }
    }

    public class WeChatPayTransfersDto
    {
        
        [Display(Name = "partner_trade_no")]
        public string PartnerTradeNo { get; set; }

        
        [Display(Name = "openid")]
        public string OpenId { get; set; }

        
        [Display(Name = "check_name")]
        public string CheckName { get; set; }

        [Display(Name = "re_user_name")]
        public string ReUserName { get; set; }

        
        [Display(Name = "amount")]
        public int Amount { get; set; }

        
        [Display(Name = "desc")]
        public string Desc { get; set; }

        
        [Display(Name = "spbill_create_ip")]
        public string SpBillCreateIp { get; set; }
    }

    public class WeChatPayGetTransferInfoDto
    {
        
        [Display(Name = "partner_trade_no")]
        public string PartnerTradeNo { get; set; }
    }

    public class WeChatPayPayBankDto
    {
        
        [Display(Name = "partner_trade_no")]
        public string PartnerTradeNo { get; set; }

        
        [Display(Name = "enc_bank_no")]
        public string EncBankNo { get; set; }

        
        [Display(Name = "enc_true_name")]
        public string EncTrueName { get; set; }

        
        [Display(Name = "bank_code")]
        public string BankCode { get; set; }

        
        [Display(Name = "amount")]
        public int Amount { get; set; }

        [Display(Name = "desc")]
        public string Desc { get; set; }
    }

    public class WeChatPayQueryBankDto
    {
        
        [Display(Name = "partner_trade_no")]
        public string PartnerTradeNo { get; set; }
    }
}
