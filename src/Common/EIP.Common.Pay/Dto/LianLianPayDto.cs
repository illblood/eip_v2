﻿using System.ComponentModel.DataAnnotations;

namespace EIP.Common.Pay.Dto
{
    public class LianLianPayWebQuickPayDto
    {
        
        [Display(Name = "no_order")]
        public string NoOrder { get; set; }

        
        [Display(Name = "dt_order")]
        public string DtOrder { get; set; }

        
        [Display(Name = "money_order")]
        public string MoneyOrder { get; set; }

        
        [Display(Name = "name_goods")]
        public string NameGoods { get; set; }

        
        [Display(Name = "user_id")]
        public string UserId { get; set; }

        
        [Display(Name = "notify_url")]
        public string NotifyUrl { get; set; }

        [Display(Name = "url_return")]
        public string UrlReturn { get; set; }

        
        [Display(Name = "risk_item")]
        public string RiskItem { get; set; }
    }

    public class LianLianPayH5QuickPayDto
    {
        
        [Display(Name = "no_order")]
        public string NoOrder { get; set; }

        
        [Display(Name = "dt_order")]
        public string DtOrder { get; set; }

        
        [Display(Name = "money_order")]
        public string MoneyOrder { get; set; }

        
        [Display(Name = "name_goods")]
        public string NameGoods { get; set; }

        
        [Display(Name = "user_id")]
        public string UserId { get; set; }

        
        [Display(Name = "notify_url")]
        public string NotifyUrl { get; set; }

        [Display(Name = "url_return")]
        public string UrlReturn { get; set; }

        
        [Display(Name = "risk_item")]
        public string RiskItem { get; set; }
    }

    public class LianLianPayOrderQueryDto
    {
        
        [Display(Name = "no_order")]
        public string NoOrder { get; set; }

        [Display(Name = "dt_order")]
        public string DtOrder { get; set; }

        [Display(Name = "oid_paybill")]
        public string OidPayBill { get; set; }
    }

    public class LianLianPayRefundDto
    {
        
        [Display(Name = "no_refund")]
        public string NoRefund { get; set; }

        
        [Display(Name = "dt_refund")]
        public string DtRefund { get; set; }

        
        [Display(Name = "money_refund")]
        public string MoneyRefund { get; set; }

        [Display(Name = "no_order")]
        public string NoOrder { get; set; }

        [Display(Name = "dt_order")]
        public string DtOrder { get; set; }

        [Display(Name = "oid_paybill")]
        public string OidPayBill { get; set; }

        
        [Display(Name = "notify_url")]
        public string NotifyUrl { get; set; }
    }

    public class LianLianPayRefundQueryDto
    {
        [Display(Name = "no_refund")]
        public string NoRefund { get; set; }

        [Display(Name = "dt_refund")]
        public string DtRefund { get; set; }

        [Display(Name = "oid_refundno")]
        public string OidRefundNo { get; set; }
    }
}
