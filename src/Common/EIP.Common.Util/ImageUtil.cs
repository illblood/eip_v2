﻿using EIP.Common.Extension;
using SixLabors.Fonts;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using System;
using System.IO;

namespace EIP.Common.Util
{
    public static class ImageUtil
    {
        /// <summary>
        ///  将echarts返回的base64 转成图片
        /// </summary>
        /// <param name="image">图片的base64形式</param>
        /// <param name="path">项目区分</param>
        public static void SaveBase64Image(string image, string path)
        {
            string filepath = Path.GetDirectoryName(path);
            // 如果不存在就创建file文件夹
            if (!Directory.Exists(filepath))
            {
                if (filepath != null) Directory.CreateDirectory(filepath);
            }
            image = image.Substring(image.IndexOf(",") + 1);
            var photoBytes = Convert.FromBase64String(image);
            System.IO.File.WriteAllBytes(path, photoBytes);
        }

        /// <summary>
        /// 合并图片 小图片放在大图片上面
        /// </summary>
        /// <param name="templeBase64Str">模板大图片base64</param>
        /// <param name="outputBase64Str">模板小图片base64</param>
        /// <returns></returns>
        public static string MergeImage(string templeBase64Str, string outputBase64Str, int x, int y, string sn)
        {
            try
            {
                byte[] templebytes = Convert.FromBase64String(templeBase64Str);
                byte[] outputbytes = Convert.FromBase64String(outputBase64Str);
                var imagesTemle = Image.Load(templebytes, out var format);
                var outputImg = Image.Load(outputbytes);
                FontCollection fonts = new FontCollection();
                SixLabors.Fonts.FontFamily fontfamily = fonts.Install(ConfigurationUtil.GetUploadPath() + "/font/msyh.ttf"); //字体的路径
                var fontName = new SixLabors.Fonts.Font(fontfamily, 30);
                var font = new SixLabors.Fonts.Font(fontfamily, 30);
                //进行多图片处理
                imagesTemle.Mutate(a =>
                {
                    a.DrawImage(outputImg, new SixLabors.Primitives.Point(x, y), 1);
                    if (sn.IsNotNullOrEmpty())
                        a.DrawText("NO." + sn, font, Rgba32.Black, new SixLabors.Primitives.PointF(x + 35, y + 210));
                    //还是合并 
                });
                var strRet = imagesTemle.ToBase64String(format);
                return strRet;
            }
            catch (Exception ex)
            {
            }

            return "";
        }

        /// <summary>
        /// 合并图片 小图片放在大图片上面
        /// </summary>
        /// <param name="templeBase64Str">模板大图片base64</param>
        /// <param name="outputBase64Str">模板小图片base64</param>
        /// <returns></returns>
        public static string MergeImagePersonalQRCode(string templeBase64Str, string outputBase64Str, int x, int y, string sn)
        {
            try
            {
                byte[] templebytes = Convert.FromBase64String(templeBase64Str);
                byte[] outputbytes = Convert.FromBase64String(outputBase64Str);
                var imagesTemle = Image.Load(templebytes, out var format);
                var outputImg = Image.Load(outputbytes);
                FontCollection fonts = new FontCollection();
                SixLabors.Fonts.FontFamily fontfamily = fonts.Install(ConfigurationUtil.GetUploadPath() + "/font/msyh.ttf"); //字体的路径
                var fontName = new SixLabors.Fonts.Font(fontfamily, 30);
                var font = new SixLabors.Fonts.Font(fontfamily, 30);
                //进行多图片处理
                imagesTemle.Mutate(a =>
                {
                    a.DrawImage(outputImg, new SixLabors.Primitives.Point(x, y), 1);
                    if (sn.IsNotNullOrEmpty())
                        a.DrawText("NO." + sn, font, Rgba32.Black, new SixLabors.Primitives.PointF(x + 102, y + 366));
                    //还是合并 
                });
                var strRet = imagesTemle.ToBase64String(format);
                return strRet;
            }
            catch (Exception ex)
            {
            }

            return "";
        }

        /// <summary>
        /// 合并图片 小图片放在大图片上面
        /// </summary>
        /// <param name="templeBase64Str">模板大图片base64</param>
        /// <param name="outputBase64Str">模板小图片base64</param>
        /// <returns></returns>
        public static string MergeImagePersonalQRCodeBrand(string templeBase64Str, string outputBase64Str, int x, int y, string sn)
        {
            try
            {
                byte[] templebytes = Convert.FromBase64String(templeBase64Str);
                byte[] outputbytes = Convert.FromBase64String(outputBase64Str);
                var imagesTemle = Image.Load(templebytes, out var format);
                var outputImg = Image.Load(outputbytes);
                FontCollection fonts = new FontCollection();
                SixLabors.Fonts.FontFamily fontfamily = fonts.Install(ConfigurationUtil.GetUploadPath() + "/font/msyh.ttf"); //字体的路径
                var fontName = new SixLabors.Fonts.Font(fontfamily, 30);
                var font = new SixLabors.Fonts.Font(fontfamily, 30);
                //进行多图片处理
                imagesTemle.Mutate(a =>
                {
                    a.DrawImage(outputImg, new SixLabors.Primitives.Point(x, y), 1);
                    if (sn.IsNotNullOrEmpty())
                        a.DrawText("NO." + sn, font, Rgba32.Black, new SixLabors.Primitives.PointF(x + 122, y + 410));
                    //还是合并 
                });
                var strRet = imagesTemle.ToBase64String(format);
                return strRet;
            }
            catch (Exception ex)
            {
            }

            return "";
        }


    }
}