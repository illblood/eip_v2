﻿using EIP.Common.Models.Attributes;
using EIP.Common.Models.Resx;
using EIP.Common.Repository.DbContexts;
using EIP.Common.Util;
using MySqlConnector;
using Npgsql;
using System;
using System.Data.SqlClient;
using System.Reflection;

namespace EIP.Common.Repository.MicroOrm
{
    public class MicroOrmUtil<TEntity> : IDisposable where TEntity : class
    {
        /// <summary>
        /// 
        /// </summary>
        public readonly string ConnectionName = "EIP";
        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="read">是否为读操作,支持读写分离,若是读操作则使用算法得到读的连接字符串</param>
        public MicroOrmUtil()
        {
            string connectionString;
            var section = ConnectionName + ":ConnectionString";
            DbAttribute m = typeof(TEntity).GetTypeInfo().GetCustomAttribute<DbAttribute>();
            if (m != null)
            {
                section = m.Name + ":ConnectionString";
            }
            connectionString = ConfigurationUtil.GetSection(section);
            ConnectionName = "EIP";
            var connectionType = ConfigurationUtil.GetSection("EIP:ConnectionType").ToLower();
            switch (connectionType)
            {
                case ResourceDataBaseType.Mysql:
                    Db = new SqlDbContext<TEntity>(new MySqlConnection(connectionString));
                    break;
                case ResourceDataBaseType.Postgresql:
                    Db = new SqlDbContext<TEntity>(new NpgsqlConnection(connectionString));
                    break;
                default://mssql
                    Db = new SqlDbContext<TEntity>(new SqlConnection(connectionString));
                    break;
            }
        }

        public SqlDbContext<TEntity> Db { get; }

        public void Dispose()
        {
            Db?.Dispose();
        }
    }
}