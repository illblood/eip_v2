﻿namespace EIP.Common.Models.Resx
{
    /// <summary>
    /// 数据库资源
    /// </summary>
    public class ResourceDataBaseType
    {
        /// <summary>
        /// Mssql
        /// </summary>
        public const string Mssql = "mssql";

        /// <summary>
        /// Mysql
        /// </summary>
        public const string Mysql = "mysql";

        /// <summary>
        /// Postgresql
        /// </summary>
        public const string Postgresql = "postgresql";

        /// <summary>
        /// MongoDb
        /// </summary>
        public const string MongoDb = "mongodb";
    }
}