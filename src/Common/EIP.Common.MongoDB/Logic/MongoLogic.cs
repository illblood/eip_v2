﻿using EIP.Common.Util;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace EIP.Common.MongoDB.Logic
{
    public class MongoLogic
    {
        private static readonly string ConnectionString = ConfigurationUtil.GetSection("MongoDb:ConnectionString");
        /// <summary>
        /// 查找
        /// </summary>
        /// <param name="Table"></param>
        /// <returns></returns>
        public static List<BsonDocument> Find(string Table)
        {
            var Option = new MongodbHostOptions();
            Option.Connection = ConnectionString;
            Option.DataBase = "foo";
            Option.Table = Table;
            var collection = MongodbClient<BsonDocument>.MongodbInfoClient(Option);
            var List = collection.Find(Builders<BsonDocument>.Filter.Empty).ToList();
            return List;
        }
        /// <summary>
        /// 查找
        /// </summary>
        /// <param name="Table"></param>
        /// <param name="Name"></param>
        /// <param name="Value"></param>
        /// <returns></returns>
        public static List<BsonDocument> Find(string Table, string Name, string Value)
        {
            var Option = new MongodbHostOptions();
            Option.Connection = ConnectionString;
            Option.DataBase = "foo";
            Option.Table = Table;
            var collection = MongodbClient<BsonDocument>.MongodbInfoClient(Option);
            var List = collection.Find(Builders<BsonDocument>.Filter.Eq(Name, Value)).ToList();
            return List;
        }
        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static void Insert(string Table, BsonDocument Model)
        {
            var Option = new MongodbHostOptions();
            Option.Connection = ConnectionString;
            Option.DataBase = "foo";
            Option.Table = Table;
            var collection = MongodbClient<BsonDocument>.MongodbInfoClient(Option);
            collection.InsertOne(Model);
        }

    }
}
