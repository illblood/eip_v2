﻿/**************************************************************
* Copyright (C) 2018 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2018/10/30 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Common.Extension.Xss;
using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace EIP.Common.Extension
{
    public static class StringExtension
    {
        /// <summary>
        /// 替换
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string RTrim(this string str)
        {
            for (int i = str.Length; i >= 0; i--)
            {
                if (str[i].Equals(" ") || str[i].Equals("\r") || str[i].Equals("\n"))
                {
                    str.Remove(i, 1);
                }
            }
            return str;
        }

        /// <summary>
        /// 判断字符串是否相等
        /// </summary>
        /// <param name="text1"></param>
        /// <param name="text2"></param>
        /// <returns></returns>
        public static bool EqualsEx(this string text1, string text2)
        {
            return string.Equals(text1, text2, StringComparison.CurrentCultureIgnoreCase);
        }

        /// <summary>
        /// 根据传入的字符串组装为符合更新或者删除sql语句in部分的字符串(字符串必须以‘,’分割)
        /// </summary>
        /// <param name="value">扩展类型</param>
        /// <returns>替换后的字符串</returns>
        /// <remarks>2015-05-15 by 孙泽伟</remarks>
        public static string InSql(this string value)
        {
            string param = "";
            if (!string.IsNullOrEmpty(value))
            {
                var strList = value.Split(',');
                param = strList.Aggregate(param, (current, str) => current + ("'" + str + "',"));
            }
            return param.TrimEnd(',');
        }

        /// <summary>
        /// 判断字符串是否空
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool IsNotNullOrEmpty(this string text)
        {
            return !string.IsNullOrEmpty(text);
        }

        /// <summary>
        /// 指示指定的字符串是 null、空还是仅由空白字符组成。
        /// </summary>
        public static bool IsNotNullOrWhiteSpace(this string value)
        {
            return !string.IsNullOrWhiteSpace(value);
        }

        /// <summary>
        /// 判断是否包含字符串
        /// </summary>
        /// <param name="text"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool ContainsEx(this string text, string value)
        {
            return text.IndexOf(value, StringComparison.CurrentCultureIgnoreCase) >= 0;
        }

        /// <summary>
        /// 判断是否以指定字符串开头
        /// </summary>
        /// <param name="text"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool StartWithEx(this string text, string value)
        {
            return text.StartsWith(value, StringComparison.CurrentCultureIgnoreCase);
        }

        /// <summary>
        /// 判断是否以指定字符串结尾
        /// </summary>
        /// <param name="text"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool EndWithEx(this string text, string value)
        {
            return text.EndsWith(value, StringComparison.CurrentCultureIgnoreCase);
        }

        /// <summary>
        /// 判断字符串是否空
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(this string text)
        {
            return string.IsNullOrEmpty(text);
        }

        /// <summary>
        /// 指示指定的字符串是 null、空还是仅由空白字符组成。
        /// </summary>
        public static bool IsNullOrWhiteSpace(this string value)
        {
            return string.IsNullOrWhiteSpace(value);
        }

        /// <summary>
        /// 以指定字符串作为分隔符将指定字符串分隔成数组
        /// </summary>
        /// <param name="value">要分割的字符串</param>
        /// <param name="strSplit">字符串类型的分隔符</param>
        /// <param name="removeEmptyEntries">是否移除数据中元素为空字符串的项</param>
        /// <returns>分割后的数据</returns>
        public static string[] Split(this string value, string strSplit, bool removeEmptyEntries = false)
        {
            return value.Split(new[] { strSplit },
                removeEmptyEntries ? StringSplitOptions.RemoveEmptyEntries : StringSplitOptions.None);
        }

        /// <summary>
        /// 替换Html标签
        /// </summary>
        /// <param name="value"></param>
        /// <param name="length"></param>
        /// <returns>替换后的字符串</returns>
        public static string ReplaceHtmlTag(this string value, int length = 0)
        {
            if (value.IsNullOrEmpty()) return value;
            string strText = Regex.Replace(value, "<[^>]+>", "");
            strText = Regex.Replace(strText, "&[^;]+;", "");
            if (length > 0 && strText.Length > length)
                return strText.Substring(0, length);
            return strText;
        }

        /// <summary>
        /// 首字母小写
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ReplaceFistLower(this string value)
        {
            return value.Substring(0, 1).ToLower() + value.Substring(1);
        }
        /// <summary>
        /// 首字母小写
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string Xss(this string value)
        {
            HtmlSanitizer sanitizer = new HtmlSanitizer();
            sanitizer = new HtmlSanitizer();
            //sanitizer.AllowedTags.Add("div");//标签白名单
            //sanitizer.AllowedAttributes.Add("class");//标签属性白名单,默认没有class标签属性      
            //sanitizer.AllowedCssProperties.Add("font-family");//CSS属性白名单
            value = sanitizer.Sanitize(value);
            return value;

        }
        /// <summary>
        /// 过滤Sql
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string FilterSql(this string sqlParameter)
        {
            if (string.IsNullOrEmpty(sqlParameter)) return string.Empty;
            sqlParameter = sqlParameter.Trim();
            sqlParameter = sqlParameter.Replace("--", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("'", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("!", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("@@", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("^", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("<", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace(">", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("%", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("=", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace(";", "", StringComparison.OrdinalIgnoreCase);

            //操作
            sqlParameter = sqlParameter.Replace("insert", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("update", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("delete", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("drop", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("exec", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("create", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("union", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("select", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("execute", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("backup", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("or", "", StringComparison.OrdinalIgnoreCase);

            //命令
            sqlParameter = sqlParameter.Replace("xp_", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("sp_", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("db_", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("is_", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("host_", "", StringComparison.OrdinalIgnoreCase);

            //表
            sqlParameter = sqlParameter.Replace("sysdatabases", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("sysobjects", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("syscolumns", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("tempdb", "", StringComparison.OrdinalIgnoreCase);

            //函数
            sqlParameter = sqlParameter.Replace("asc", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("abc", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("unicode", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("nchar", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("substring", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("use", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("count", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("len", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("ascii", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("cast", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("exists", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("is_member", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("is_srvrolemember", "", StringComparison.OrdinalIgnoreCase);

            //关键词
            sqlParameter = sqlParameter.Replace("and", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("where", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("xtype", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("inner", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("join", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("output ", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("with", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("master", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("truncate", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("declare", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("array", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("alter", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("database", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("set", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("dbid", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("top", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("delay ", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("waitfor", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("order", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("sysadmin", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("for", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("@echo", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("procedure", "", StringComparison.OrdinalIgnoreCase);
            sqlParameter = sqlParameter.Replace("assembly", "", StringComparison.OrdinalIgnoreCase);
            return sqlParameter.Xss();
        }

        /// <summary>
        /// 反解密
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string UnEscape(this string value)
        {
            return HttpUtility.UrlDecode(value);
        }
        /// <summary>
        /// 将Base64字符串转换为图片并保存:base64字符串图片为png
        /// </summary>
        /// <param name="value"></param>
        /// <param name="path">保存图片的路径</param>
        public static void ConvertBase64ToImage(this string value, string path)
        {
            string filepath = Path.GetDirectoryName(path);
            // 如果不存在就创建file文件夹  
            if (!Directory.Exists(filepath))
            {
                if (filepath != null) Directory.CreateDirectory(filepath);
            }
            var match = Regex.Match(value, "data:image/png;base64,([\\w\\W]*)$");
            if (match.Success)
            {
                value = match.Groups[1].Value;
            }
            var photoBytes = Convert.FromBase64String(value);
            File.WriteAllBytes(path, photoBytes);
        }
    }
}