﻿/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Agile.Logic;
using EIP.Agile.Models.Dtos.Config;
using EIP.Base.Models.Entities.Agile;
using EIP.Common.Controller.Attribute;
using EIP.Common.Models.Dtos;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace EIP.Agile.Controller
{
    /// <summary>
    /// 敏捷开发
    /// </summary>
    public class ConfigController : BaseAgileController
    {
        #region 构造函数
        private readonly IAgileConfigLogic _agileConfigLogic;
        /// <summary>
        /// 
        /// </summary>
        public ConfigController(
            IAgileConfigLogic agileConfigLogic
            )
        {
            _agileConfigLogic = agileConfigLogic;
        }

        #endregion

        /// <summary>
        /// 分页获取
        /// </summary>
        /// <param name="paging">分页参数</param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("敏捷开发-方法-列表-根据父级查询所有子集", RemarkFrom.System)]
        [Route("/agile/config/list")]
        public async Task<JsonResult> Find(AgileConfigFindInput paging)
        {
            return JsonForGridPaging(await _agileConfigLogic.Find(paging));
        }

        /// <summary>
        /// 根据Id获取
        /// </summary>
        /// <param name="input">主键信息</param>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Remark("敏捷开发-方法-编辑-根据Id获取", RemarkFrom.System)]
        [Route("/agile/config/{id}")]
        public async Task<JsonResult> FindById([FromRoute] IdInput input)
        {
            return Json(await _agileConfigLogic.FindById(input));
        }

        /// <summary>
        /// 根据Id获取
        /// </summary>
        /// <param name="input">主键信息</param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("敏捷开发-方法-编辑-根据Id获取", RemarkFrom.System)]
        [Route("/agile/config/menu")]
        public async Task<JsonResult> FindByMenuId( AgileConfigFindByMenuIdInput input)
        {
            return Json(await _agileConfigLogic.FindByMenuId(input));
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="input">主键信息</param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("敏捷开发-方法-编辑-保存", RemarkFrom.System)]
        [Route("/agile/config")]
        public async Task<JsonResult> Save(AgileConfig input)
        {
            return Json(await _agileConfigLogic.Save(input));
        }
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="input">主键信息</param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("敏捷开发-方法-编辑-保存", RemarkFrom.System)]
        [Route("/agile/config/type")]
        public async Task<JsonResult> SaveType(AgileConfig input)
        {
            return Json(await _agileConfigLogic.SaveType(input));
        }
        /// <summary>
        /// 保存设计信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("敏捷开发-方法-保存设计信息", RemarkFrom.Workflow)]
        [Route("/agile/config/save/json")]
        public async Task<JsonResult> SaveJson(AgileConfig input)
        {
            return Json(await _agileConfigLogic.SaveJson(input));
        }

        /// <summary>
        /// 发布设计信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("敏捷开发-方法-发布设计信息", RemarkFrom.Workflow)]
        [Route("/agile/config/public/json")]
        public async Task<JsonResult> PublicJson(AgileConfig input)
        {
            return Json(await _agileConfigLogic.PublicJson(input));
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="input">主键集合</param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("敏捷开发-方法-列表-删除", RemarkFrom.System)]
        [Route("/agile/config/delete")]
        public async Task<JsonResult> Delete(IdInput<string> input)
        {
            return Json(await _agileConfigLogic.Delete(input));
        }

        /// <summary>
        /// 分页获取
        /// </summary>
        /// <param name="paging">分页参数</param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("敏捷开发-方法-根据类型查询表单配置", RemarkFrom.System)]
        [Route("/agile/config/base")]
        public async Task<JsonResult> FindBase(AgileConfigFindBaseInput paging)
        {
            return Json(await _agileConfigLogic.FindBase(paging));
        }

        /// <summary>
        /// 获取表单字段
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Remark("敏捷开发-方法-获取表单字段", RemarkFrom.System)]
        [Route("/agile/config/columns/{id}")]
        public async Task<JsonResult> FindFormColumns([FromRoute] IdInput input)
        {
            return Json(await _agileConfigLogic.FindFormColumns(input));
        }

    }
}