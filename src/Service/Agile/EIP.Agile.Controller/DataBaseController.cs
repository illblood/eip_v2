﻿/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Agile.Logic;
using EIP.Agile.Models.Dtos.DataBase;
using EIP.Common.Controller.Attribute;
using EIP.Common.Models.Dtos.DataBase;
using EIP.Common.Models.Tree;
using EIP.Common.Util;
using EIP.System.Logic;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EIP.Agile.Controller
{
    /// <summary>
    /// 数据库控制器
    /// </summary>
    public class DataBaseController : BaseAgileController
    {
        #region 构造函数
        private readonly IAgileDataBaseLogic _agileDataBaseLogic;
        private readonly ISystemPermissionLogic _permissionLogic;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="agileDataBaseLogic"></param>
        /// <param name="permissionLogic"></param>
        public DataBaseController(IAgileDataBaseLogic agileDataBaseLogic, ISystemPermissionLogic permissionLogic)
        {
            _agileDataBaseLogic = agileDataBaseLogic;
            _permissionLogic = permissionLogic;
        }

        #endregion

        #region 方法
        /// <summary>
        /// 获取所有应用数据库
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Remark("应用数据库-方法-列表-获取所有应用数据库", RemarkFrom.System)]
        [Route("/agile/database/table/tree")]
        public async Task<JsonResult> FindTableTree()
        {
            return JsonForTree((await _agileDataBaseLogic.FindTableTree()).Data);
        }

        /// <summary>
        /// 获取表空间使用情况
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Remark("应用数据库-方法-列表-获取表空间使用情况", RemarkFrom.System)]
        [Route("/agile/database/spaceused")]
        public async Task<JsonResult> FindDataBaseSpaceused()
        {
            return Json(await _agileDataBaseLogic.FindDataBaseSpaceused());
        }

        /// <summary>
        /// 获取对应数据库视图信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Remark("应用数据库-方法-列表-获取对应数据库表信息", RemarkFrom.System)]
        [Route("/agile/database/view")]
        public async Task<JsonResult> FindDataBaseView()
        {
            return Json(await _agileDataBaseLogic.FindDataBaseView());
        }

        /// <summary>
        /// 获取对应数据库视图信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Remark("应用数据库-方法-列表-获取对应数据库表信息", RemarkFrom.System)]
        [Route("/agile/database/proc")]
        public async Task<JsonResult> FindDataBaseProc()
        {
            return Json(await _agileDataBaseLogic.FindDataBaseProc());
        }
        /// <summary>
        /// 获取对应数据库表信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Remark("应用数据库-方法-列表-获取对应数据库表信息", RemarkFrom.System)]
        [Route("/agile/database/table")]
        public async Task<JsonResult> FindDataBaseTable()
        {
            return Json(await _agileDataBaseLogic.FindDataBaseTable());
        }

        /// <summary>
        /// 获取对应表列信息
        /// </summary>
        /// <param name="doubleWayDto"></param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("应用数据库-方法-列表-获取对应表列信息", RemarkFrom.System)]
        [Route("/agile/database/column")]
        public async Task<JsonResult> FindDataBaseColumnsList(AgileDataBaseTableDto doubleWayDto)
        {
            return Json(await _agileDataBaseLogic.FindDataBaseColumns(doubleWayDto));
        }

        /// <summary>
        /// 表是否存在
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("应用数据库-方法-列表-获取对应表列信息", RemarkFrom.System)]
        [Route("/agile/database/tableexist")]
        public async Task<JsonResult> IsTableExist(AgileDataBaseIsTableExistInput input)
        {
            return Json(await _agileDataBaseLogic.IsTableExist(input));
        }

        /// <summary>
        /// 获取字段树结构
        /// </summary>
        /// <param name="doubleWay"></param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("应用数据库-方法-列表-获取对应表列信息树结构", RemarkFrom.System)]
        public async Task<JsonResult> FindDataBaseColumnsTree(AgileDataBaseTableDto doubleWay)
        {
            var columns = (await _agileDataBaseLogic.FindDataBaseColumns(doubleWay)).Data;
            IList<JsTreeEntity> treeEntities = new List<JsTreeEntity>();
            var parentId = CombUtil.NewComb();
            JsTreeEntity treeEntity = new JsTreeEntity
            {
                id = parentId,
                text = doubleWay.Name
            };
            treeEntities.Add(treeEntity);
            foreach (var co in columns)
            {
                treeEntity = new JsTreeEntity
                {
                    parent = parentId,
                    text = "(" + co.Name + ")",
                    id = co.Name
                };
                treeEntities.Add(treeEntity);
            }
            return Json(treeEntities);
        }

        /// <summary>
        /// 获取外键信息
        /// </summary>
        /// <param name="doubleWayDto"></param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("应用数据库-方法-列表-获取外键信息", RemarkFrom.System)]
        public async Task<JsonResult> FindDataBasefFkColumn(AgileDataBaseTableDto doubleWayDto)
        {
            return Json(await _agileDataBaseLogic.FindDataBasefFkColumn(doubleWayDto));
        }

        /// <summary>
        /// 创建表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("表单维护-方法-创建表", RemarkFrom.Workflow)]
        [Route("/agile/database/table")]
        public JsonResult SaveFormTable(AgileDataBaseSaveFormTableInput input)
        {
            return Json(_agileDataBaseLogic.SaveFormTable(input));
        }

        /// <summary>
        /// 修改表字段
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("表单维护-方法-修改表字段", RemarkFrom.Workflow)]
        [Route("/agile/database/tablefield")]
        public async Task<JsonResult> SaveFormTableField(AgileDataBaseSaveFormTableFieldInput input)
        {
            return Json(await _agileDataBaseLogic.SaveFormTableField(input));
        }

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="input">信息</param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("业务代码生成器-方法-新增/编辑-保存", RemarkFrom.System)]
        [Route("/agile/database/businessdata")]
        public async Task<JsonResult> SaveBusinessData(AgileDataBaseSaveBusinessDataInput input)
        {
            input.UserId = CurrentUser.UserId;
            input.UserCode = CurrentUser.Code;
            input.UserName = CurrentUser.Name;
            input.OrganizationName = CurrentUser.OrganizationName;
            if (CurrentUser.OrganizationId != null) input.OrganizationId = (Guid)CurrentUser.OrganizationId;
            return Json(await _agileDataBaseLogic.SaveBusinessData(input));
        }

        /// <summary>
        /// 根据主键获取信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("业务代码生成器-方法-列表-根据主键获取信息", RemarkFrom.System)]
        [Route("/agile/database/businessdata/byid")]
        public async Task<JsonResult> FindBusinessDataById(AgileDataBaseFindBusinessDataByIdInput input)
        {
            return Json(await _agileDataBaseLogic.FindBusinessDataById(input));
        }
        /// <summary>
        /// 删除业务数据
        /// </summary>
        /// <param name="input">Id</param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("业务代码生成器-方法-列表-删除", RemarkFrom.System)]
        [Route("/agile/database/businessdata/del")]
        public async Task<JsonResult> DeleteBusinessData(AgileDataBaseDeleteBusinessDataInput input)
        {
            return Json(await _agileDataBaseLogic.DeleteBusinessData(input));
        }
     
        /// <summary>
        /// 获取所有信息
        /// </summary>
        /// <param name="input">获取所有信息</param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("业务代码生成器-方法-列表-获取所有信息", RemarkFrom.System)]
        [Route("/agile/database/businessdata/list")]
        public async Task<JsonResult> FindBusinessData(AgileDataBaseFindPagingBusinessDataInput input)
        {
            if (input.HaveDataPermission)
            {
                input.Rote.UserId = CurrentUser.UserId;
                input.DataSql = (await _permissionLogic.FindDataPermissionSql(input.Rote)).Data;
            }
            return Json(await _agileDataBaseLogic.FindBusinessData(input));
        }

        /// <summary>
        ///获取子表数据
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("业务代码生成器-方法-获取子表数据", RemarkFrom.Workflow)]
        [Route("/agile/database/businessdata/batch/list")]
        public async Task<JsonResult> FindBatchData(DataBaseSubTableDto input)
        {
            return Json(await _agileDataBaseLogic.FindFromSubTable(input));
        }

        /// <summary>
        /// 获取指定信息
        /// </summary>
        /// <param name="input">获取所有信息</param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("业务代码生成器-方法-列表-获取指定信息", RemarkFrom.System)]
        [Route("/agile/database/businessdata/fromsource")]
        public JsonResult FindFormSourceData(AgileDataBaseFindFormSourceDataInput input)
        {
            return Json(_agileDataBaseLogic.FindFormSourceData(input));
        }

        /// <summary>
        /// 获取指定信息
        /// </summary>
        /// <param name="input">获取所有信息</param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("业务代码生成器-方法-列表-获取指定信息", RemarkFrom.System)]
        [Route("/agile/database/businessdata/fromsourcepaging")]
        public JsonResult FindFormSourceDataPaging(AgileDataBaseFindFormSourceDataInput input)
        {
            return Json(_agileDataBaseLogic.FindFormSourceDataPaging(input));
        }
        #endregion
    }
}