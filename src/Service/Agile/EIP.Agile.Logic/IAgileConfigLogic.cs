/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2018/11/9 9:21:04
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EasyCaching.Core.Interceptor;
using EIP.Agile.Models.Dtos.Config;
using EIP.Base.Models.Entities.Agile;
using EIP.Common.Logic;
using EIP.Common.Models;
using EIP.Common.Models.Dtos;
using EIP.Common.Models.Paging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EIP.Agile.Logic
{
    /// <summary>
    /// 敏捷开发业务逻辑接口
    /// </summary>
    public interface IAgileConfigLogic : IAsyncLogic<AgileConfig>
    {
        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="paging"></param>
        [EasyCachingAble(CacheKeyPrefix = "IAgileConfigLogic_Cache")]
        Task<OperateStatus<PagedResults<AgileConfig>>> Find(AgileConfigFindInput paging);

        /// <summary>
        /// 根据Id获取
        /// </summary>
        /// <param name="paging"></param>
        [EasyCachingAble(CacheKeyPrefix = "IAgileConfigLogic_Cache")]
        Task<OperateStatus<AgileConfig>> FindById(IdInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [EasyCachingAble(CacheKeyPrefix = "IAgileConfigLogic_Cache")]
        Task<OperateStatus<IEnumerable<AgileConfigFindByMenuIdOutput>>> FindByMenuId(AgileConfigFindByMenuIdInput input);

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="input">实体</param>
        /// <returns></returns>
        [EasyCachingEvict(IsAll = true, CacheKeyPrefix = "IAgileConfigLogic_Cache")]
        Task<OperateStatus> Save(AgileConfig input);
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="input">实体</param>
        /// <returns></returns>
        [EasyCachingEvict(IsAll = true, CacheKeyPrefix = "IAgileConfigLogic_Cache")]
        Task<OperateStatus> SaveType(AgileConfig input);

        /// <summary>
        /// 保存表单设计器基本信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [EasyCachingEvict(IsAll = true, CacheKeyPrefix = "IAgileConfigLogic_Cache")]
        Task<OperateStatus> SaveJson(AgileConfig input);

        /// <summary>
        /// 保存表单设计器基本信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [EasyCachingEvict(IsAll = true, CacheKeyPrefix = "IAgileConfigLogic_Cache")]
        Task<OperateStatus> PublicJson(AgileConfig input);

        /// <summary>
        /// 删除
        /// </summary>
        /// <returns></returns>
        [EasyCachingEvict(IsAll = true, CacheKeyPrefix = "IAgileConfigLogic_Cache")]
        Task<OperateStatus> Delete(IdInput<string> input);

        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="paging"></param>
        [EasyCachingAble(CacheKeyPrefix = "IAgileConfigLogic_Cache")]
        Task<OperateStatus<IEnumerable<AgileConfigFindBaseOutput>>> FindBase(AgileConfigFindBaseInput input);

        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="input"></param>
        [EasyCachingAble(CacheKeyPrefix = "IAgileConfigLogic_Cache")]
        Task<OperateStatus<List<AgileConfigFindFormColumnsOutput>>> FindFormColumns(IdInput input);
    }
}