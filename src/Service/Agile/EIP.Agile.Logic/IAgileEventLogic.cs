﻿/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/

using EIP.Agile.Models.Dtos.Event;

namespace EIP.Agile.Logic
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAgileEventLogic
    {
        /// <summary>
        /// 根据Api查询
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        object EventDoByApi(AgileEventDoByApiInput input);
    }
}