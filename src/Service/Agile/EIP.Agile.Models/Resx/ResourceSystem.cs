﻿/**************************************************************
* Copyright (C) 2018 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2018/10/30 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
namespace EIP.Agile.Models.Resx
{
    /// <summary>
    /// 系统资源说明
    /// </summary>
    public class ResourceAgile
    {
        /// <summary>
        /// 请先配置数据库连接字符串
        /// </summary>
        public const string 请先配置数据库连接字符串 = "请先在EIP.System.Api项目下appsetting.json中添加连接字符串,如:'EIP_Hr': {'ConnectionString': 'server=.;database=EIP;uid=sa;pwd=123456'}";

        /// <summary>
        /// 数据库打开失败
        /// </summary>
        public const string 数据库打开失败 = "数据库打开失败:{0}";

        /// <summary>
        /// 已存在相同代码
        /// </summary>
        public const string 已存在相同代码 = "已存在相同代码";

        /// <summary>
        /// 请配置子表详细信息
        /// </summary>
        public const string 请配置子表详细信息 = "请配置子表详细信息";
    }
}