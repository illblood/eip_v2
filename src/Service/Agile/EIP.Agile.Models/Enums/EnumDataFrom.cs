﻿namespace EIP.Agile.Models.Enums
{

    /// <summary>
    /// 数据来源枚举
    /// </summary>
    public enum EnumDataFrom
    {
        /// <summary>
        /// 表
        /// </summary>
        表 = 1,

        /// <summary>
        /// 视图
        /// </summary>
        视图 = 2,

        /// <summary>
        /// 存储过程
        /// </summary>
        存储过程 = 3,
    }
}
