﻿using EIP.Common.Models.Paging;
using System;

namespace EIP.Agile.Models.Dtos.Config
{
    /// <summary>
    /// 
    /// </summary>
    public class AgileConfigFindInput : QueryParam
    {
        /// <summary>
        /// 表单类别
        /// </summary>
        public Guid? TypeId { get; set; }

        /// <summary>
        /// 生成类型:1列表配置,2表单配置
        /// </summary>
        public short? ConfigType { get; set; }

        /// <summary>
        /// 表单类型:1设计器,2自定义
        /// </summary>
        public short? FormCategory { get; set; }

        /// <summary>
        /// 使用类型:1敏捷开发,2流程表单
        /// </summary>
        public short? UseType { get; set; }

        /// <summary>
        /// 配置Id
        /// </summary>
        public Guid? ConfigId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool MenuIdNull { get; set; } = true;
    }
}