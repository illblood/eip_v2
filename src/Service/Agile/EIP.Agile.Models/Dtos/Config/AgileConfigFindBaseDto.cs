﻿using System;

namespace EIP.Agile.Models.Dtos.Config
{
    /// <summary>
    /// 
    /// </summary>
    public class AgileConfigFindBaseInput
    {
        /// <summary>
        /// 使用类型:1敏捷开发,2流程表单
        /// </summary>
        public int? UseType { get; set; }

        /// <summary>
        /// 生成类型:1列表配置,2表单配置
        /// </summary>
        public int? ConfigType { get; set; }

        /// <summary>
        /// 配置Id
        /// </summary>
        public Guid? ConfigId { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class AgileConfigFindBaseOutput
    {
        /// <summary>
        /// 配置Id
        /// </summary>
        public Guid ConfigId { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }
}
