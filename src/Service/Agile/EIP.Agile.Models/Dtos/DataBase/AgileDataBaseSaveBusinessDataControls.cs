﻿/**************************************************************
* Copyright (C) 2018 www.sf-info.cn 盛峰版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2018/10/30 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using System.Collections.Generic;

namespace EIP.Agile.Models.Dtos.DataBase
{
    /// <summary>
    /// 表单控件
    /// </summary>
    public class AgileDataBaseSaveBusinessDataColumns
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// 默认值
        /// </summary>
        public string Default { get; set; }

        /// <summary>
        /// 单选
        /// </summary>
        public bool IsSingle { get; set; }

        /// <summary>
        /// 是否删除:App端如扫码新增可能不需要删除
        /// </summary>
        public bool IsDelete { get; set; } = true;

        /// <summary>
        /// 子表
        /// </summary>
        public IList<DataBaseFormSubtableControl> Subtable { get; set; }=new List<DataBaseFormSubtableControl>();
    }

    /// <summary>
    /// 子表
    /// </summary>
    public class DataBaseFormSubtableControl
    {

    }

}
