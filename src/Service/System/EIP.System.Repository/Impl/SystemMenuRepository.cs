using EIP.Common.Extension;
using EIP.Common.Models.Paging;
using EIP.Common.Models.Tree;
using EIP.Common.Repository;
using EIP.System.Models.Dtos.Menu;
using EIP.System.Models.Enums;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EIP.System.Repository.Impl
{
    /// <summary>
    /// 模块
    /// </summary>
    public class SystemMenuRepository : ISystemMenuRepository
    {
        /// <summary>
        /// 根据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public Task<PagedResults<SystemMenuFindOutput>> Find(SystemMenuFindInput input)
        {
            var sql = new StringBuilder();
            sql.Append("select menu.CreateTime,menu.CreateUserName,menu.UpdateTime,menu.UpdateUserName, menu.MenuId,menu.Name,menu.Icon,menu.Theme,menu.OpenType,menu.Path,menu.CanbeDelete," +
                "menu.Remark,menu.OrderNo,menu.HaveMenuPermission," +
                "menu.HaveDataPermission,menu.HaveFieldPermission," +
                "menu.HaveButtonPermission,menu.IsFreeze,menu.IsShowMenu,menu.ParentIdsName,@rowNumber, @recordCount from System_Menu menu  @where ");
            if (input.Id.HasValue)
            {
                sql.Append(input.HaveSelf
                    ? "AND menu.ParentIds like '%" + input.Id + "%'"
                    : "AND menu.ParentIds like '%" + (input.Id + ",%'"));
            }
            if (input.Sidx.IsNullOrEmpty())
            {
                input.Sidx = " menu.OrderNo";
            }
            return new SqlMapperUtil().PagingQuerySqlAsync<SystemMenuFindOutput>(sql.ToString(), input);
        }

        /// <summary>
        /// 获取树形结构菜单
        /// </summary>
        /// <param name="privilegeAccess"></param>
        /// <returns></returns>
        public Task<IEnumerable<BaseTree>> FindPermissionMenu(EnumPrivilegeAccess privilegeAccess)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append(
                "select MenuId id,ParentId parent,Name text,icon,theme from System_Menu where IsFreeze=0");
            switch (privilegeAccess)
            {
                case EnumPrivilegeAccess.模块权限:
                    sql.Append(" and HaveMenuPermission=1 ");
                    break;
                case EnumPrivilegeAccess.模块按钮:
                    sql.Append(" and HaveButtonPermission=1 ");
                    break;
                case EnumPrivilegeAccess.数据权限:
                    sql.Append(" and HaveDataPermission=1 ");
                    break;
                case EnumPrivilegeAccess.字段权限:
                    sql.Append(" and HaveFieldPermission=1 ");
                    break;
            }
            sql.Append(" order by OrderNo ");
            return new SqlMapperUtil().SqlWithParams<BaseTree>(sql.ToString());
        }
    }
}