using EIP.Common.Models.Paging;
using EIP.System.Models.Dtos.Notice;
using System.Threading.Tasks;

namespace EIP.System.Repository.IRepository
{
    /// <summary>
    /// 公告
    /// </summary>
    public interface ISystemNoticeRepository
    {
        /// <summary>
        /// 获取公告信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PagedResults<SystemNoticeFindPagingOutput>> FindPaging(SystemNoticeFindPagingInput input);
    }
}
