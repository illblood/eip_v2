﻿using EIP.Base.Models.Entities.System;
using EIP.Common.Models.Paging;
using EIP.System.Models.Dtos.Type;
using System.Threading.Tasks;

namespace EIP.System.Repository
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISystemTypeRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="paging"></param>
        /// <returns></returns>
        Task<PagedResults<SystemType>> Find(SystemTypeFindInput paging);
    }
}