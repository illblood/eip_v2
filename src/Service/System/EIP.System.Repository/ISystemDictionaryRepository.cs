using EIP.Common.Models.Paging;
using EIP.System.Models.Dtos.Dictionary;
using System.Threading.Tasks;

namespace EIP.System.Repository
{
    /// <summary>
    /// 字典数据访问接口
    /// </summary>
    public interface ISystemDictionaryRepository
    {
        /// <summary>
        /// 根据父级查询下级
        /// </summary>
        /// <param name="input">父级id</param>
        /// <returns></returns>
        Task<PagedResults<SystemDictionaryFindOutput>> Find(SystemDictionaryFindInput input);
    }
}