﻿/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Base.Models.Entities.System;
using EIP.Common.Controller.Attribute;
using EIP.Common.Models.Dtos;
using EIP.System.Logic;
using EIP.System.Models.Dtos.Type;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace EIP.System.Controller
{
    /// <summary>
    /// 
    /// </summary>
    public class TypeController : BaseSystemController
    {
        #region 构造函数
        private readonly ISystemTypeLogic _systemTypeLogic;
        /// <summary>
        /// 
        /// </summary>
        public TypeController(ISystemTypeLogic systemTypeLogic)
        {
            _systemTypeLogic = systemTypeLogic;
        }
        #endregion

        /// <summary>
        /// 分页获取
        /// </summary>
        /// <param name="paging">分页参数</param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("系统类别-方法-列表-分页获取", RemarkFrom.System)]
        [Route("/system/type/list")]
        public async Task<JsonResult> Find(SystemTypeFindInput paging)
        {
            return JsonForGridPaging(await _systemTypeLogic.Find(paging));
        }

        /// <summary>
        /// 分页获取
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("系统类别-方法-获取所有", RemarkFrom.System)]
        [Route("/system/type/all")]
        public async Task<JsonResult> FindAll()
        {
            return Json(await _systemTypeLogic.FindAll());
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="input">主键信息</param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("系统类别-方法-编辑-保存", RemarkFrom.System)]
        [Route("/system/type")]
        public async Task<JsonResult> Save(SystemType input)
        {
            return Json(await _systemTypeLogic.Save(input));
        }
        /// <summary>
        /// 根据Id获取
        /// </summary>
        /// <param name="input">主键信息</param>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Remark("系统类别-方法-编辑-根据Id获取", RemarkFrom.System)]
        [Route("/system/type/{id}")]
        public async Task<JsonResult> FindById([FromRoute] IdInput input)
        {
            return Json(await _systemTypeLogic.FindById(input));
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="input">主键集合</param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("系统类别-方法-列表-删除", RemarkFrom.System)]
        [Route("/system/type/delete")]
        public async Task<JsonResult> Delete(IdInput<string> input)
        {
            return Json(await _systemTypeLogic.Delete(input));
        }

        /// <summary>
        /// 冻结
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("系统类别-方法-冻结", RemarkFrom.System)]
        [Route("/system/type/isfreeze")]
        public async Task<JsonResult> IsFreeze(IdInput input)
        {
            return Json(await _systemTypeLogic.IsFreeze(input));
        }
    }
}