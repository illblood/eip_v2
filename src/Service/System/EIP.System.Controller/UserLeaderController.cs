﻿/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Base.Models.Resx;
using EIP.Common.Controller.Attribute;
using EIP.Common.Extension;
using EIP.Common.Models;
using EIP.Common.Models.Dtos;
using EIP.System.Logic;
using EIP.System.Models.Dtos.Permission;
using EIP.System.Models.Dtos.User;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EIP.System.Controller
{
    /// <summary>
    /// 用户管理:此用户为系统使用人员,系统的人员管理在其他模块进行管理(如:人事管理HR)
    /// 此模块只维护基础信息
    /// </summary>

    public class UserLeaderController : BaseSystemController
    {
        #region 构造函数
        private readonly ISystemPermissionLogic _permissionLogic;
        private readonly ISystemUserLeaderLogic _userLeaderLogic;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userLeaderLogic"></param>
        /// <param name="permissionLogic"></param>        
        public UserLeaderController(ISystemUserLeaderLogic userLeaderLogic,
            ISystemPermissionLogic permissionLogic)
        {
            _userLeaderLogic = userLeaderLogic;
            _permissionLogic = permissionLogic;
        }

        #endregion

        #region 方法
        /// <summary>
        /// 获取所有用户上级
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [CreateBy("孙泽伟")]
        [Remark("用户上级-视图-获取所有用户上级", RemarkFrom.System)]
        [HttpPost]
        public async Task<JsonResult> Find(IdInput input)
        {
            SystemUserChosenInput chosenInput = new SystemUserChosenInput();
            #region 获取权限控制器信息
            chosenInput.DataSql = (await _permissionLogic.FindDataPermissionSql(new ViewRote
            {
                UserId = CurrentUser.UserId,
                MenuId = ResourceMenuId.系统用户.ToGuid()
            })).Data;
            #endregion
            chosenInput.UserId = input.Id;
            return Json(await _userLeaderLogic.GetUserLeader(chosenInput));
        }

        /// <summary>
        /// 保存所有上级
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userLeader"></param>
        /// <returns></returns>
        [CreateBy("孙泽伟")]
        [Remark("用户上级-视图-保存所有上级", RemarkFrom.System)]
        [HttpPost]
        public async Task<JsonResult> Save(Guid userId, string userLeader)
        {
            return Json(await _userLeaderLogic.Save(userId, JsonConvert.DeserializeObject<IList<SystemPermissionSaveUserInput>>(userLeader).Select(m => m.U).ToList()));
        }

        /// <summary>
        /// 保存所有下级
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userSubordinate"></param>
        /// <returns></returns>
        [CreateBy("孙泽伟")]
        [Remark("用户上级-视图-保存所有下级", RemarkFrom.System)]
        [HttpPost]
        public async Task<JsonResult> SaveSubordinate(Guid userId, string userSubordinate)
        {
            return Json(await _userLeaderLogic.SaveSubordinate(userId, JsonConvert.DeserializeObject<IList<SystemPermissionSaveUserInput>>(userSubordinate).Select(m => m.U).ToList()));
        }

        /// <summary>
        /// 获取所有用户下级
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [CreateBy("孙泽伟")]
        [Remark("用户上级-视图-获取所有用户下级", RemarkFrom.System)]
        [HttpPost]
        public async Task<JsonResult> FindSubordinate(IdInput input)
        {
            SystemUserChosenInput chosenInput = new SystemUserChosenInput();
            #region 获取权限控制器信息
            chosenInput.DataSql = (await _permissionLogic.FindDataPermissionSql(new ViewRote
            {
                UserId = CurrentUser.UserId,
                MenuId = ResourceMenuId.系统用户.ToGuid()
            })).Data;
            #endregion
            chosenInput.UserId = input.Id;
            return Json(await _userLeaderLogic.FindSubordinate(chosenInput));
        }
        /// <summary>
        /// 获取所有用户下级
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [CreateBy("孙泽伟")]
        [Remark("用户上级-视图-获取所有用户下级", RemarkFrom.System)]
        [HttpPost]
        public async Task<JsonResult> FindSubordinateLoadOnce(IdInput input)
        {
            return Json(await _userLeaderLogic.FindSubordinate(input));
        }
        #endregion
    }
}