using EIP.Base.Models.Entities.System;
using EIP.Common.Controller.Attribute;
using EIP.Common.Models.Dtos;
using EIP.System.Logic.Permission.ILogic;
using EIP.System.Models.Dtos.MobileMenu;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace EIP.System.Controller
{
    /// <summary>
    /// 移动端菜单
    /// </summary>
    public class MobileMenuController : BaseSystemController
    {
        #region 构造函数
        private readonly ISystemMobileMenuLogic _systemMobileMenuLogic;
        /// <summary>
        /// 移动端菜单构造函数
        /// </summary>
        /// <param name="systemMobileMenuLogic"></param>
        public MobileMenuController(ISystemMobileMenuLogic systemMobileMenuLogic)
        {
            _systemMobileMenuLogic = systemMobileMenuLogic;
        }

        #endregion

        #region 方法

        /// <summary>
        /// 获取所有模块信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Remark("模块维护-方法-列表-获取所有模块信息", RemarkFrom.System)]
        [Route("/system/mobilemenu")]
        public async Task<JsonResult> Tree()
        {
            return JsonForTree((await _systemMobileMenuLogic.Tree()).Data);
        }

        /// <summary>
        /// 根据父级Id获取下级模块
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("模块维护-方法-列表-根据父级Id获取下级模块", RemarkFrom.System)]
        [Route("/system/mobilemenu/list")]
        public async Task<JsonResult> Find(SystemMobileMenuFindInput input)
        {
            return JsonForGridPaging(await _systemMobileMenuLogic.Find(input));
        }

        /// <summary>
        /// 根据Id获取
        /// </summary>
        /// <param name="input">主键信息</param>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Description("移动端菜单-方法-编辑-根据Id获取")]
        [Route("/system/mobilemenu/{id}")]
        public async Task<JsonResult> FindById([FromRoute] IdInput input)
        {
            return Json(await _systemMobileMenuLogic.FindById(input));
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="input">主键信息</param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Description("移动端菜单-方法-编辑-保存")]
        [Route("/system/mobilemenu")]
        public async Task<JsonResult> Save(SystemMobileMenu input)
        {
            return Json(await _systemMobileMenuLogic.Save(input));
        }
        /// <summary>
        /// 是否具有模块权限
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("模块维护-方法-是否具有模块权限", RemarkFrom.System)]
        [Route("/system/mobilemenu/havemenupermission")]
        public async Task<JsonResult> HaveMenuPermission(IdInput input)
        {
            return Json(await _systemMobileMenuLogic.HaveMenuPermission(input));
        }

        /// <summary>
        /// 是否具有数据权限
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("模块维护-方法-是否具有数据权限", RemarkFrom.System)]
        [Route("/system/mobilemenu/havedatapermission")]
        public async Task<JsonResult> HaveDataPermission(IdInput input)
        {
            return Json(await _systemMobileMenuLogic.HaveDataPermission(input));
        }

        /// <summary>
        /// 是否具有字段权限
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("模块维护-方法-是否具有字段权限", RemarkFrom.System)]
        [Route("/system/mobilemenu/havefieldpermission")]
        public async Task<JsonResult> HaveFieldPermission(IdInput input)
        {
            return Json(await _systemMobileMenuLogic.HaveFieldPermission(input));
        }

        /// <summary>
        /// 是否具有功能项权限
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("模块维护-方法-是否具有功能项权限", RemarkFrom.System)]
        [Route("/system/mobilemenu/havebuttonpermission")]
        public async Task<JsonResult> HaveButtonPermission(IdInput input)
        {
            return Json(await _systemMobileMenuLogic.HaveButtonPermission(input));
        }

        /// <summary>
        /// 冻结
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("模块维护-方法-冻结", RemarkFrom.System)]
        [Route("/system/mobilemenu/isfreeze")]
        public async Task<JsonResult> IsFreeze(IdInput input)
        {
            return Json(await _systemMobileMenuLogic.IsFreeze(input));
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="input">主键集合</param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Description("移动端菜单-方法-列表-删除")]
        [Route("/system/mobilemenu/delete")]
        public async Task<JsonResult> Delete(IdInput<string> input)
        {
            return Json(await _systemMobileMenuLogic.Delete(input));
        }

        /// <summary>
        /// 查询具有移动按钮菜单
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Description("移动端菜单-方法-编辑-查询具有移动按钮菜单")]
        [Route("/system/mobilemenu/buttonpermission")]
        public async Task<JsonResult> FindHaveMenuButtonPermissionMenu()
        {
            return JsonForTree((await _systemMobileMenuLogic.FindHaveMenuButtonPermissionMenu()).ToList());
        }

        #endregion
    }
}
