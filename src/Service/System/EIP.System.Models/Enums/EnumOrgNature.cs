﻿/**************************************************************
* Copyright (C) 2018 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2018/10/30 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
namespace EIP.System.Models.Enums
{
    /// <summary>
    /// 类型
    /// </summary>
    public enum EnumOrgNature
    {
        /// <summary>
        /// 智慧社区
        /// </summary>
        智慧社区,
        /// <summary>
        /// 省
        /// </summary>
        省,
        /// <summary>
        /// 市
        /// </summary>
        市,
        /// <summary>
        /// 区县
        /// </summary>
        区县,
        /// <summary>
        /// 镇
        /// </summary>
        镇,
        /// <summary>
        /// 社区街道
        /// </summary>
        社区街道,
        /// <summary>
        /// 单位
        /// </summary>
        单位
    }
}