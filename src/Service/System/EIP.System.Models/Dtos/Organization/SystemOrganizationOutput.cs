﻿/**************************************************************
* Copyright (C) 2018 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2018/10/30 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Common.Util;
using EIP.Base.Models.Entities.System;
using EIP.System.Models.Enums;

namespace EIP.System.Models.Dtos.Organization
{
    /// <summary>
    /// 组织机构输出
    /// </summary>
    public class SystemOrganizationOutput : SystemOrganization
    {
        /// <summary>
        /// 性质名称
        /// </summary>
        public string NatureName => Nature != null ? EnumUtil.FindEnumNameByIndex<EnumOrgNature>((int)Nature) : string.Empty;
    }
}