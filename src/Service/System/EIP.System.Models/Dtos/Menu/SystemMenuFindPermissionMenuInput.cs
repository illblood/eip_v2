﻿using EIP.System.Models.Enums;

namespace EIP.System.Models.Dtos.Permission
{
    /// <summary>
    /// 
    /// </summary>
    public class SystemMenuFindPermissionMenuInput
    {
        /// <summary>
        /// 权限类型
        /// </summary>
        public EnumPrivilegeAccess PrivilegeAccess { get; set; }
        
    }
}