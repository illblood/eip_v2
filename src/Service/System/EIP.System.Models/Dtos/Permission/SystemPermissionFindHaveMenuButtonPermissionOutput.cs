﻿using System;

namespace EIP.System.Models.Dtos.Permission
{
    /// <summary>
    /// 
    /// </summary>
    public class SystemPermissionFindHaveMenuButtonPermissionOutput
    {
        /// <summary>
        /// 
        /// </summary>
        public Guid MenuId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Guid MenuButtonId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int OrderNo { get; set; }


    }
}