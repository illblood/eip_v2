﻿using System;

namespace EIP.Agile.Models.Dtos.Sn
{
    /// <summary>
    /// 
    /// </summary>
    public class SystemSnInput
    {
        /// <summary>
        /// 规则
        /// </summary>
        public string Code { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class SystemSnOutput
    {
        /// <summary>
        /// 规则
        /// </summary>
        public string Value { get; set; }
    }
}
