/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2018/11/9 9:21:04
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EasyCaching.Core.Interceptor;
using EIP.Base.Models.Entities.System;
using EIP.Common.Logic;
using EIP.Common.Models;
using EIP.Common.Models.Dtos;
using EIP.Common.Models.Paging;
using EIP.System.Models.Dtos.Type;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EIP.System.Logic
{
    /// <summary>
    /// 敏捷开发业务逻辑接口
    /// </summary>
    public interface ISystemTypeLogic : IAsyncLogic<SystemType>
    {
        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="paging"></param>
        [EasyCachingAble(CacheKeyPrefix = "ISystemTypeLogic_Cache")]
        Task<OperateStatus<PagedResults<SystemType>>> Find(SystemTypeFindInput paging);

        /// <summary>
        /// 获取所有
        /// </summary>
        /// <param name="paging"></param>
        [EasyCachingAble(CacheKeyPrefix = "ISystemTypeLogic_Cache")]
        Task<OperateStatus<IEnumerable<SystemType>>> FindAll();

        /// <summary>
        /// 根据Id获取
        /// </summary>
        /// <param name="paging"></param>
        [EasyCachingAble(CacheKeyPrefix = "ISystemTypeLogic_Cache")]
        Task<OperateStatus<SystemType>> FindById(IdInput input);

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="input">实体</param>
        /// <returns></returns>
        [EasyCachingEvict(IsAll = true, CacheKeyPrefix = "ISystemTypeLogic_Cache")]
        Task<OperateStatus> Save(SystemType input);

        /// <summary>
        /// 删除
        /// </summary>
        /// <returns></returns>
        [EasyCachingEvict(IsAll = true, CacheKeyPrefix = "ISystemTypeLogic_Cache")]
        Task<OperateStatus> Delete(IdInput<string> input);

        /// <summary>
        /// 冻结
        /// </summary>
        /// <param name="input">Id</param>
        /// <returns></returns>
        [EasyCachingEvict(IsAll = true, CacheKeyPrefix = "ISystemTypeLogic_Cache")]
        Task<OperateStatus> IsFreeze(IdInput input);
    }
}