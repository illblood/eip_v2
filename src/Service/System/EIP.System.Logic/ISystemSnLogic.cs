/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Agile.Models.Dtos.Sn;
using EIP.Base.Models.Entities.System;
using EIP.Common.Logic;
using EIP.Common.Models;
using System.Threading.Tasks;

namespace EIP.System.Logic
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISystemSnLogic : IAsyncLogic<SystemSn>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<OperateStatus<SystemSnOutput>> Find(SystemSnInput input);
    }
}