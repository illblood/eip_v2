/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2018/11/9 9:21:04
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Base.Models.Entities.System;
using EIP.Common.Core.Context;
using EIP.Common.Logic;
using EIP.Common.Models;
using EIP.Common.Models.Dtos;
using EIP.Common.Models.Paging;
using EIP.System.Models.Dtos.Type;
using EIP.System.Repository;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EIP.System.Logic.Impl
{
    /// <summary>
    /// 敏捷开发业务逻辑接口实现
    /// </summary>
    public class SystemTypeLogic : DapperAsyncLogic<SystemType>, ISystemTypeLogic
    {
        #region 构造函数
        private readonly ISystemTypeRepository _systemTypeRepository;

        /// <summary>
        /// 模块
        /// </summary>
        /// <param name="systemTypeRepository"></param>
        public SystemTypeLogic(ISystemTypeRepository systemTypeRepository)
        {
            _systemTypeRepository = systemTypeRepository;
        }

        #endregion

        #region 方法

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="input">实体</param>
        /// <returns></returns>
        public async Task<OperateStatus> Save(SystemType input)
        {
            OperateStatus operateStatus = new OperateStatus();
            SystemType systemType = await FindAsync(f => f.TypeId == input.TypeId);
            var currentUser = EipHttpContext.CurrentUser();
            if (systemType == null)
            {
                input.CreateTime = DateTime.Now;
                input.CreateUserId = currentUser.UserId;
                input.CreateUserName = currentUser.Name;
                input.UpdateTime = DateTime.Now;
                input.UpdateUserId = currentUser.UserId;
                input.UpdateUserName = currentUser.Name;
                operateStatus = await InsertAsync(input);
            }
            else
            {
                systemType.OrderNo = input.OrderNo;
                systemType.Name = input.Name;
                systemType.IsFreeze = input.IsFreeze;
                systemType.Remark = input.Remark;
                systemType.UpdateTime = DateTime.Now;
                systemType.UpdateUserId = currentUser.UserId;
                systemType.UpdateUserName = currentUser.Name;
                operateStatus = await UpdateAsync(systemType);
            }
            if (operateStatus.Code == ResultCode.Success)
            {
                operateStatus.Msg = "保存成功";
            }
            return operateStatus;
        }


        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="paging"></param>
        /// <returns></returns>
        public async Task<OperateStatus<PagedResults<SystemType>>> Find(SystemTypeFindInput paging)
        {
            return OperateStatus<PagedResults<SystemType>>.Success(await _systemTypeRepository.Find(paging));
        }

        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="paging"></param>
        /// <returns></returns>
        public async Task<OperateStatus<IEnumerable<SystemType>>> FindAll()
        {
            return OperateStatus<IEnumerable<SystemType>>.Success(await FindAllAsync(f => f.IsFreeze == false));
        }

        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="paging"></param>
        /// <returns></returns>
        public async Task<OperateStatus<SystemType>> FindById(IdInput input)
        {
            return OperateStatus<SystemType>.Success(await FindAsync(f => f.TypeId == input.Id));
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<OperateStatus> Delete(IdInput<string> input)
        {
            foreach (var id in input.Id.Split(','))
            {
                var configId = Guid.Parse(id);
                var config = await FindAsync(f => f.TypeId == configId);
                await DeleteAsync(config);
            }
            return OperateStatus.Success();
        }

        /// <summary>
        /// 冻结
        /// </summary>
        /// <returns></returns>
        public async Task<OperateStatus> IsFreeze(IdInput input)
        {
            var type = await FindAsync(f => f.TypeId == input.Id);
            type.IsFreeze = !type.IsFreeze;
            return await UpdateAsync(type);
        }

        #endregion
    }
}