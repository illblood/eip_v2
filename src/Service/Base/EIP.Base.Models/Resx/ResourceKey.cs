﻿namespace EIP.Base.Models.Resx
{
    public  class ResourceKey
    {
        /// <summary>
        /// 小程序加密私钥
        /// </summary>
        public static string 小程序加密私钥 = @"MIICWwIBAAKBgQCJnmjLNnNPf/Dl+gIQ6OY8QB3vP2Uew9tc2p0JbSbOxbdE5lQf
GMq8jfzl52OudZX7nPEIXvTT4z9I0iF0KrGVZjKIhuCPFW1YnZNvBVejK+cN7lYZ
qwKPbh/S2fnyh2oqnjwQLog1QLnfaqAEwEyXir3U57dMhZXVPaxKsuJR4wIDAQAB
AoGAJ3RP7PrGYmN2yCg+EWUEJP/o1R+iuQhDysIuRXaSqrNKkKlMMsIj1Z0LhUoq
bOjvP/zFqbW3kUvUZ+c+ihR3ZsIAhw+ybubdRwrTVpWU0OvOpvS47BLth44m/pAa
/0h79oJ91zhadx8UZz4Tw9jub6reTKgoyaHWL3j7wnZqXskCQQCKTsEK2ONadoJV
6j5q2C6Tk1+B5bWHPdSwLBSPw9Hso8Vvc9iDRAsX/eOZUhUQqGV19ONFHYaXadFc
9qhm902NAkEA/rmYa7NS7qQwaGkCs7ZX/NgiFMAPLSJeEbKNNhfjYYT31gKvrODY
e8WAzfrWpcJoX2VWiR2qeyp1KtOI5a2pLwJAWMO0l06dk7iNa7B7wLzgH6ys1kRk
HdksAKk5xQ7rUtHE7APQiGaKLCqxAPyyMk+xVmQF3JzXfaPXT6+qYvMDHQJADUXw
ilQq0yKdHyOxr1mmFhfRFtG3OLvcuQFb2GDASXmbTHyRWOnSfgiZoKIcg0CxHZ9z
Nevk0UwyFMw3CvjUowJAeEpfclUq0CXWcxz1KXsZMR9F1RNEl8vqW2pFEuyokzuT
Z4z3AYcQXUKXgtG3QLjX3nRiuAtF4wxI+FMDCz3iQQ==";

        /// <summary>
        /// 小程序加密公钥
        /// </summary>
        public static string 小程序加密公钥 = @"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCJnmjLNnNPf/Dl+gIQ6OY8QB3v
P2Uew9tc2p0JbSbOxbdE5lQfGMq8jfzl52OudZX7nPEIXvTT4z9I0iF0KrGVZjKI
huCPFW1YnZNvBVejK+cN7lYZqwKPbh/S2fnyh2oqnjwQLog1QLnfaqAEwEyXir3U
57dMhZXVPaxKsuJR4wIDAQAB";

        /// <summary>
        /// 后台加密私钥P@ssw9rd
        /// </summary>
        public static string 后台加密私钥 = @"MIICXAIBAAKBgQC9ds1UxO/ARslpqzBA4AIY7ejOJyYoe980wYSpMpfYb/cLyHl6
6c4uqm567CrigYxkt1Nv24h4L8Cy4v2aJm6T/Oi46a5DRHfbYFyHGwzan+iUqsNb
37xPHkTd1KDuRYbvZsYZD15wUHPWczECXdj2FO0C4VDx6kriwJQv9PoqnwIDAQAB
AoGAQpcQz+xvv4qhm8wZJ5QT6KMlod9MoAn5h1dEwxsPF3kywSdy2up2Fxy7u+Rp
pjaWG4f7gWMNcgCGn9j7/UPotOqf/7L8x86YaRnqN3UOfnNVbp/wHM9XBxLI+uiy
jDjaTSTWxbyksjy3DBS0clIyaDlb4K9IwB015AROfv/+htECQQD6tM/ZJMcAkvZo
4R0wuwM1OcQwSUNrvIRra2oTM9naheeUZsHEhx3+caO5XiKmMg8mlVQoT8FenzBW
hoTd//azAkEAwXbyTOzbpw0E8v/mT7PC5vYC2qY7ZtHdN3YNSbv6njB3FR/DMZAj
P3p3DrqW6dtgstv1/rfMp0Fu8zd8pmXSZQJANCTtYi2rEmx+wduolZ0gEq78Jkrm
sf7Yz/rcajgLpM7bFtu0i6rKy0RZmFGDBWw+CHlgsq27+3FCvYxnEEGPzwJBAJcQ
sCbNFL8edWzh0Q8PSPNbyeK9xsiEuv4hmkKnHA0FvxaplJtQ95ULpYfEvy6gdKN1
CmL5Vj6L43zbKOQtwdkCQFRZYK7WuFlZdOhUH/jmqzmrF4ENhhqbr7G/xra+5C65
/4H1twiA+r6UOcCTGzSSjLnRbZlLMdcVvPGeSZAwu6E=";

        /// <summary>
        /// 后台加密公钥
        /// </summary>
        public static string 后台加密公钥 = @"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC9ds1UxO/ARslpqzBA4AIY7ejO
JyYoe980wYSpMpfYb/cLyHl66c4uqm567CrigYxkt1Nv24h4L8Cy4v2aJm6T/Oi4
6a5DRHfbYFyHGwzan+iUqsNb37xPHkTd1KDuRYbvZsYZD15wUHPWczECXdj2FO0C
4VDx6kriwJQv9PoqnwIDAQAB";

    }
}
