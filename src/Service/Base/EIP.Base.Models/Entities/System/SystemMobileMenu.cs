using EIP.Common.Models.Attributes.MicroOrm;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace EIP.Base.Models.Entities.System
{
    /// <summary>
    /// 移动端菜单
    /// </summary>
    [Serializable]
	[Table("System_MobileMenu")]
    public class SystemMobileMenu
    {
        /// <summary>
        /// 消息Id
        /// </summary>		
        [Key, Identity, JsonIgnore]
        public long Id { get; set; }

        /// <summary>
        /// 主键id
        /// </summary>		
        public Guid MobileMenuId{ get; set; }
       
        /// <summary>
        /// 父级id
        /// </summary>		
		public Guid? ParentId{ get; set; }
       
        /// <summary>
        /// 
        /// </summary>		
		public string ParentName{ get; set; }
       
        /// <summary>
        /// 名称
        /// </summary>		
		public string Name{ get; set; }

        /// <summary>
        /// 图标
        /// </summary>		
        public string Icon { get; set; }

        /// <summary>
        /// 打开类型
        /// </summary>		
		public byte? OpenType{ get; set; }
       
        /// <summary>
        /// 
        /// </summary>		
		public string Path{ get; set; }
       
        /// <summary>
        /// 
        /// </summary>		
		public string Remark{ get; set; }
       
        /// <summary>
        /// 
        /// </summary>		
		public int OrderNo{ get; set; }
       
        /// <summary>
        /// 打开地址
        /// </summary>		
		public bool HaveMenuPermission{ get; set; } = false;

        /// <summary>
        /// 备注
        /// </summary>		
        public bool HaveDataPermission{ get; set; } = false;

        /// <summary>
        /// 排序
        /// </summary>		
        public bool HaveFieldPermission{ get; set; } = false;

        /// <summary>
        /// 
        /// </summary>		
        public bool HaveButtonPermission{ get; set; } = false;

        /// <summary>
        /// 
        /// </summary>		
        public bool IsFreeze { get; set; } = false;

        /// <summary>
        /// 
        /// </summary>
        public string ParentIds { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ParentIdsName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>		
        public DateTime CreateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 创建用户Id
        /// </summary>		
        public Guid CreateUserId { get; set; }

        /// <summary>
        /// 创建人员名称
        /// </summary>
        public string CreateUserName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>		
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 创建用户Id
        /// </summary>		
        public Guid? UpdateUserId { get; set; }

        /// <summary>
        /// 修改人员名称
        /// </summary>
        public string UpdateUserName { get; set; }
    }
}
