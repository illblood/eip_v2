﻿/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Common.Models.Attributes.MicroOrm;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace EIP.Base.Models.Entities.WeChat
{
    /// <summary>
    /// 企业微信用户信息
    /// </summary>
    [Serializable]
    [Table("WeChat_Work_User")]
    public class WeChatWorkUser
    {
        /// <summary>
        /// 自增Id
        /// </summary>
        [Key, Identity, JsonIgnore]
        public int Id { get; set; }

        /// <summary>
        /// 主键Id
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// 企业微信组织Id
        /// </summary>
        public long DeptId { get; set; }

        /// <summary>
        /// 企业微信组织名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 企业微信用户Id
        /// </summary>
        public string WeChatWorkUserId { get; set; }

        /// <summary>
        /// 用户在当前开发者企业帐号范围内的唯一标识。
        /// </summary>
        public string UnionId { get; set; }

        /// <summary>
        /// 头像地址。
        /// </summary>
        public string Avatar { get; set; }

        /// <summary>
        /// 电话号码
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// 职位
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 所属部门id列表
        /// </summary>
        public string DeptIdList { get; set; }

        /// <summary>
        /// 所属部门名称列表
        /// </summary>
        public string DeptNameList { get; set; }

        /// <summary>
        /// 系统用户Id
        /// </summary>
        public Guid? SystemUserId { get; set; }

        /// <summary>
        /// 系统组织架构Id
        /// </summary>
        public Guid SystemOrganizationId { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>		
        public DateTime CreateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 创建用户Id
        /// </summary>		
        public Guid CreateUserId { get; set; }

        /// <summary>
        /// 创建人员名称
        /// </summary>
        public string CreateUserName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>		
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 创建用户Id
        /// </summary>		
        public Guid? UpdateUserId { get; set; }

        /// <summary>
        /// 修改人员名称
        /// </summary>
        public string UpdateUserName { get; set; }
    }
}
