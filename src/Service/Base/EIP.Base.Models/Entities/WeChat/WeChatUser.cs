﻿/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Common.Models.Attributes.MicroOrm;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace EIP.Base.Models.Entities.WeChat
{
    /// <summary>
    /// 微信授权用户信息
    /// </summary>
    [Serializable]
    [Table("wechat_user")]
    public class WeChatUser
    {
        /// <summary>
        /// 自增Id
        /// </summary>
        [Key, Identity, JsonIgnore]
        public int Id { get; set; }

        /// <summary>
        /// 主键Id
        /// </summary>
        public Guid WeChatUserId { get; set; }

        /// <summary>
        /// OpenId
        /// </summary>
        public string OpenId { get; set; }

        /// <summary>
        /// 公众平台Id
        /// </summary>
        public string UnionId { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// 头像路径
        /// </summary>
        public string HeadImgurl { get; set; }

        /// <summary>
        /// 授权时间
        /// </summary>
        public DateTime? CreateTime { get; set; }

        /// <summary>
        /// 最后一次授权时间
        /// </summary>
        public DateTime? LastTime { get; set; }

        /// <summary>
        /// 电话号码
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 主键
        /// </summary>
        public Guid? UserId { get; set; }

        /// <summary>
        /// 关注公众号
        /// </summary>
        public int Subscribe { get; set; }

        /// <summary>
        /// 类型:1公众号,2小程序,3企业微信
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 小程序sessionId
        /// </summary>
        public string SessionId { get; set; }

        /// <summary>
        /// 小程序SessionKey
        /// </summary>
        public string SessionKey { get; set; }
    }
}
