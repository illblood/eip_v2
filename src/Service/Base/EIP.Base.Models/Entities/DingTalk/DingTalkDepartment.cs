﻿using EIP.Common.Models.Attributes.MicroOrm;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace EIP.Base.Models.Entities.DingTalk
{
    /// <summary>
    /// 钉钉组织架构
    /// </summary>
    [Serializable]
    [Table("DingTalk_Department")]
    public class DingTalkDepartment
    {
        /// <summary>
        /// 自增Id
        /// </summary>
        [Key, Identity, JsonIgnore]
        public int Id { get; set; }

        /// <summary>
        /// 主键Id
        /// </summary>
        public Guid DepartmentId { get; set; }

        /// <summary>
        /// 钉钉组织Id
        /// </summary>
        public string DeptId { get; set; }

        /// <summary>
        /// 钉钉组织名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 钉钉父级Id
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// 对应系统Id
        /// </summary>
        public Guid OrganizationId { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>		
        public DateTime CreateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 创建用户Id
        /// </summary>		
        public Guid CreateUserId { get; set; }

        /// <summary>
        /// 创建人员名称
        /// </summary>
        public string CreateUserName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>		
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 创建用户Id
        /// </summary>		
        public Guid? UpdateUserId { get; set; }

        /// <summary>
        /// 修改人员名称
        /// </summary>
        public string UpdateUserName { get; set; }

        /// <summary>
        /// 性质:0集团,1公司,2分公司,3子公司,4部门
        /// </summary>
        public int? Nature { get; set; }

        /// <summary>
        /// 魔方组织Id，魔方教育使用,获取人员时直接使用
        /// </summary>
        public string CubeOrgId { get; set; }

    }
}
