﻿using EIP.Common.Message.DingTalk.Dto;
using EIP.Common.Message.Email.Dto;
using EIP.Common.Message.Sms.Dto;
using EIP.Common.Message.WebSite.Dto;
using System.Threading.Tasks;

namespace EIP.Workflow.Logic
{
    public interface IWorkflowMessageLogic
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        Task SendSms(SendSmsInput message);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        void SendEmail(SendEmailInput message);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        Task SendWebSite(SendWebSiteInput message);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        void SendOfficialAccount(SendOfficialAccountInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task SendDingTalk(SendDingTalkInput input);
    }
}