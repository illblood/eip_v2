/**************************************************************
* Copyright (C) 2018 www.sf-info.cn 盛峰版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2018/10/30 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Base.Models.Entities.Workflow;
using EIP.Common.Extension;
using EIP.Common.Models;
using EIP.Common.Models.Dtos;
using EIP.Common.Models.Resx;
using EIP.Common.Repository;
using EIP.Common.Util;
using EIP.Workflow.Engine.Models.Dtos;
using EIP.Workflow.Engine.Models.Dtos.ProcessInstance;
using OfficeOpenXml.FormulaParsing.Excel.Functions.RefAndLookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EIP.Workflow.Engine.Repository
{
    /// <summary>
    /// 
    /// </summary>
    public class WorkflowProcessInstanceActivityRepository : IWorkflowProcessInstanceActivityRepository
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public Task<IEnumerable<WorkflowEngineFindActivityByProcessIdAndTypeOutput>> FindActivityByProcessIdAndType(WorkflowEngineFindActivityByProcessIdAndTypeInput input)
        {
            string sql = @"SELECT
	                            activity.ActivityId,
	                            activity.ProcessId,
	                            activity.FormId,
	                            activity.Type,
	                            activity.Json,
	                            config.PublicJson,
                                config.ColumnJson 
                            FROM
	                            Workflow_ProcessActivity activity
	                            LEFT JOIN Workflow_Process process ON process.ProcessId = activity.ProcessId
	                            LEFT JOIN agile_config config ON config.ConfigId = activity.FormId
                           where activity.ProcessId = @processId and activity.Type = @type";
            return new SqlMapperUtil().SqlWithParams<WorkflowEngineFindActivityByProcessIdAndTypeOutput>(sql, new
            {
                processId = input.ProcessId,
                type = input.Type.FindDescription()
            });
        }

        /// <summary>
        /// 根据任务Id获取表单信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public Task<WorkflowEngineFindActivityByTaskIdOutput> FindActivityByTaskId(IdInput input)
        {
            string sql = @"select  activity.FormId,activity.Json,activity.ActivityId,agile.ColumnJson from Workflow_ProcessInstance_Activity activity
            left join Workflow_ProcessInstance_Task task on task.ActivityId=activity.ActivityId 
            left join agile_config agile on activity.FormId = agile.ConfigId
			where task.TaskId=@taskId";
            return new SqlMapperUtil().SqlWithParamsSingle<WorkflowEngineFindActivityByTaskIdOutput>(sql, new
            {
                taskId = input.Id
            });
        }

        /// <summary>
        /// 根据实例Id获取表单信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public Task<WorkflowEngineFindFormByProcessInstanceIdOutput> FindWorkflowProcessFormByProcessInstanceId(IdInput input)
        {
            string sql = @"select form.Url,form.PublicHtml from  Workflow_Process process 
                            left join Workflow_ProcessInstance pinstance on process.ProcessId=pinstance.ProcessId
                            left join Workflow_Form form on process.FormId = form.FormId
                            where pinstance.ProcessInstanceId=@processInstanceId";
            return new SqlMapperUtil().SqlWithParamsSingle<WorkflowEngineFindFormByProcessInstanceIdOutput>(sql, new
            {
                processInstanceId = input.Id
            });
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public Task<IEnumerable<WorkflowButton>> FindProcessButtonByActivity(IdInput input)
        {
            string sql = @" declare @str varchar(1024)  --定义变量
                            select  @str=Button from Workflow_ProcessActivity where ActivityId=@activityId
                            select * from Workflow_Button where ButtonId in(select  str2table  from strtotable(@str)) order by OrderNo";
            return new SqlMapperUtil().SqlWithParams<WorkflowButton>(sql, new
            {
                activityId = input.Id
            });
        }

        /// <summary>
        /// 获取流程实例按钮
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public Task<IEnumerable<WorkflowButton>> FindProcessInstanceButtonByActivity(IdInput input)
        {
            string sql = @" declare @str varchar(1024)  --定义变量
                            select  @str=Button from Workflow_ProcessInstance_Activity where ActivityId=@activityId
                            select * from Workflow_Button where ButtonId in(select  str2table  from strtotable(@str)) order by OrderNo";
            return new SqlMapperUtil().SqlWithParams<WorkflowButton>(sql, new
            {
                activityId = input.Id
            });
        }

        /// <summary>
        /// 获取流程实例活动
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public Task<IEnumerable<WorkflowEngineFindReturnDoUserOutput>> FindReturnDoUser(WorkflowEngineFindReturnActivityInput input)
        {
            string sql = @" select DoUserId,DoUserCode,DoUserName,activity.Title from Workflow_ProcessInstance_Task task
                            left join Workflow_ProcessInstance_Activity activity on task.ProcessInstanceId=activity.ProcessInstanceId and task.ActivityId=activity.ActivityId
                            where task.ActivityId=@activityId and task.ProcessInstanceId=@processInstanceId  and DoUserId is not null group by DoUserId,DoUserCode,DoUserName,activity.Title";
            return new SqlMapperUtil().SqlWithParams<WorkflowEngineFindReturnDoUserOutput>(sql, new
            {
                activityId = input.ActivityId,
                processInstanceId = input.ProcessInstanceId
            });
        }
        private string connectionString = ConfigurationUtil.GetSection("EIP:ConnectionString");
        private string connectionType = ConfigurationUtil.GetSection("EIP:ConnectionType").ToLower();
        /// <summary>
        /// 检测条件
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<bool> CheckCondition(WorkflowEngineConditionInput input)
        {
            string[] noCreateField = { "batch", "uploadimg", "uploadfile", "wps" };
            //filterSql字段，是否过滤数据字段
            string[] filterSqlField = { "switch", "sign" };
            string connectionType = ConfigurationUtil.GetSection("EIP:ConnectionType").ToLower();
            string tempSql = "";
            switch (connectionType)
            {
                case ResourceDataBaseType.Mysql:
                    tempSql = $"drop TABLE if exists " + input.Table + "_CheckConditionTemp;CREATE TEMPORARY TABLE " + input.Table + "_CheckConditionTemp" + " SELECT * FROM " + input.Table + " LIMIT 0;";
                    StringBuilder stringBuilder = new StringBuilder();
                    StringBuilder stringBuilderValues = new StringBuilder();
                    stringBuilder.Append($"INSERT INTO {input.Table + "_CheckConditionTemp"} (");
                    //主表
                    var insertControls = input.Columns.Where(item => item.Name.ToUpper() != "RELATIONID"
                                                                                             && !item.Value.IsNullOrEmpty()
                                                                                             && !noCreateField.Contains(item.Type.ToLower()));
                    foreach (var item in insertControls)
                    {
                        stringBuilder.Append($"{item.Name},");
                        item.Value = filterSqlField.Any(f => f == item.Type) ? item.Value : item.Value.FilterSql().Xss();
                        stringBuilderValues.Append(item.Type == "switch" ? $"{item.Value}," : $"N'{item.Value}',");
                    }
                    stringBuilder.Append("RelationId,");
                    stringBuilderValues.Append($"'{Guid.NewGuid()}',");

                    stringBuilder.Append("CreateUserId,");
                    stringBuilderValues.Append($"'{Guid.NewGuid()}',");

                    stringBuilder.Append("CreateUserName,");
                    stringBuilderValues.Append($"'',");

                    stringBuilder.Append("CreateOrganizationId,");
                    stringBuilderValues.Append($"'{Guid.NewGuid()}',");

                    stringBuilder.Append("CreateOrganizationName,");
                    stringBuilderValues.Append($"'',");

                    stringBuilder.Append("UpdateUserId,");
                    stringBuilderValues.Append($"'{Guid.NewGuid()}',");

                    stringBuilder.Append("UpdateUserName,");
                    stringBuilderValues.Append($"'',");

                    stringBuilder.Append("UpdateOrganizationId,");
                    stringBuilderValues.Append($"'{Guid.NewGuid()}',");

                    stringBuilder.Append("UpdateOrganizationName,");
                    stringBuilderValues.Append($"'',");
                    var sql = stringBuilder.ToString().TrimEnd(',') + " ) VALUES (" +
                           stringBuilderValues.ToString().TrimEnd(',') + ")";
                    tempSql += sql + ";";
                    input.Judge = input.Judge.UnEscape();
                    tempSql += $"SELECT * FROM {input.Table}_CheckConditionTemp WHERE {input.Judge};";
                    break;
                case ResourceDataBaseType.Postgresql:
                    break;
                default:
                    //SqlServer
                    break;
            }
            //StringBuilder stringBuilder = new StringBuilder();
            //stringBuilder.Append("SELECT * FROM ");
            //stringBuilder.Append(input.Table);
            //stringBuilder.Append(" WHERE ");
            //stringBuilder.Append(input.Judge.UnEscape().Xss());
            //stringBuilder.Append((input.Judge.UnEscape().Xss().IsNotNullOrEmpty() ? " And " : "") + $" RelationId='{input.ProcessInstanceId}'");
            return await new SqlMapperUtil(string.Empty, connectionType, connectionString).SqlWithParamsBool<object>(tempSql, new { });
        }

        /// <summary>
        /// 连线检测条件
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<bool> CheckConditionLink(WorkflowEngineConditionInput input)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("SELECT * FROM ");
            stringBuilder.Append(input.Table);
            stringBuilder.Append(" WHERE ");
            stringBuilder.Append(input.Judge.UnEscape().FilterSql());
            return await new SqlMapperUtil(string.Empty, connectionType, connectionString).SqlWithParamsBool<object>(stringBuilder.ToString(), new { });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public Task<IEnumerable<WorkflowEngineFindReturnActivityOutput>> FindReturnActivity(WorkflowEngineFindReturnActivityInput input)
        {
            string sql = @" select activity.ActivityId,activity.Title from Workflow_ProcessInstance_Activity activity
                            where activity.ActivityId in (select ActivityId from Workflow_ProcessInstance_Task task where task.ProcessInstanceId=@processInstanceId and task.TaskId!=@taskId and task.DoUserId is not null) and activity.ProcessInstanceId=@processInstanceId";
            return new SqlMapperUtil().SqlWithParams<WorkflowEngineFindReturnActivityOutput>(sql, new
            {
                processInstanceId = input.ProcessInstanceId,
                taskId = input.TaskId
            });
        }

        /// <summary>
        /// 更新通过状态
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<int> UpdateIsPass(List<Guid> input)
        {
            return await new SqlMapperUtil().InsertUpdateOrDeleteSql<int>($"UPDATE Workflow_ProcessInstance_Activity SET IsPass=1 WHERE ActivityId IN ({input.Select(s => s).ExpandAndToString().InSql()})");
        }
    }
}