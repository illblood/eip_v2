﻿/**************************************************************
* Copyright (C) 2018 www.sf-info.cn 盛峰版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2018/10/30 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/

using EIP.Agile.Models.Dtos.DataBase;
using EIP.Common.Models;
using EIP.Common.Models.Dtos;
using EIP.Workflow.Engine.Models.Dtos;
using EIP.Workflow.Engine.Models.Dtos.Form;
using EIP.Workflow.Engine.Models.Dtos.ProcessInstance;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EIP.Workflow.Engine.Logic
{
    /// <summary>
    /// 
    /// </summary>
    public interface IWorkflowEngineFormLogic
    {
        /// <summary>
        /// 初始化表单数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<IList<WorkflowEngineFormControlsOutput>> InitFormData(WorkflowEngineFormInitDataInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataBaseId"></param>
        /// <param name="sql"></param>
        /// <returns></returns>
        Task<IEnumerable<WorkflowEngineProcessingPersonDetailOutput>> GetApproveUserBySql(Guid? dataBaseId, string sql);

        /// <summary>
        /// 根据实例Id获取列值
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<OperateStatus<IEnumerable<AgileDataBaseColumnDto>>> FindDataBaseColumnsByProcessId(IdInput input);

        /// <summary>
        /// 删除表单数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<OperateStatus> DeleteFormData(IList<WorkflowEngineFormDeleteDataInput> input);

        /// <summary>
        /// 根据Api查询
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<object> EventDoByApi(WorkflowEngineEventDoByApiInput input);
    }
}