﻿/**************************************************************
* Copyright (C) 2022 www.eipflow.com 孙泽伟版权所有(盗版必究)
*
* 作者: 孙泽伟(QQ 1039318332)
* 创建时间: 2022/01/12 22:40:15
* 文件名: 
* 描述: 
* 
* 修改历史
* 修改人：
* 时间：
* 修改说明：
*
**************************************************************/
using EIP.Base.Models.Entities.Workflow;
using EIP.Common.Controller.Attribute;
using EIP.Common.Models.Dtos;
using EIP.Workflow.Logic;
using EIP.Workflow.Models.Dtos.Process;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace EIP.Workflow.Controller
{
    /// <summary>
    /// 工作流控制器
    /// </summary>
    public class ProcessController : BaseWorkflowController
    {
        #region 构造函数
        private readonly IWorkflowProcessLogic _workflowProcessLogic;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="workflowProcessLogic"></param>
        public ProcessController(IWorkflowProcessLogic workflowProcessLogic)
        {
            _workflowProcessLogic = workflowProcessLogic;
        }
        #endregion

        #region 方法
        /// <summary>
        /// 获取所有流程
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("流程维护-方法-根据流程类型获取所有流程", RemarkFrom.Workflow)]
        [Route("/workflow/process/list")]
        public async Task<JsonResult> Find(WorkflowProcessFindInput input)
        {
            return JsonForGridPaging(await _workflowProcessLogic.Find(input));
        }

        /// <summary>
        /// 获取所有流程
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("流程维护-方法-根据流程类型获取所有历史版本流程", RemarkFrom.Workflow)]
        public async Task<JsonResult> FindVersionWorkflow(IdInput input)
        {
            return Json(await _workflowProcessLogic.FindVersion(input));
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("流程维护-方法-保存流程信息", RemarkFrom.Workflow)]
        [Route("/workflow/process")]
        public async Task<JsonResult> Save(WorkflowProcess process)
        {
            return Json(await _workflowProcessLogic.Save(process));
        }

        /// <summary>
        /// 复制
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("流程维护-方法-保存流程信息", RemarkFrom.Workflow)]
        [Route("/workflow/process/copy")]
        public async Task<JsonResult> Copy(WorkflowProcess process)
        {
            process.CreateUserId = CurrentUser.UserId;
            process.CreateUserName = CurrentUser.Name;
            return Json(await _workflowProcessLogic.Copy(process));
        }

        /// <summary>
        /// 保存流程设计Json
        /// </summary>
        /// <param name="process"></param>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("流程维护-方法-保存流程设计Json", RemarkFrom.Workflow)]
        [Route("/workflow/process/json")]
        public async Task<JsonResult> SaveDesignJson(WorkflowProcessSaveInput process)
        {
            process.UpdateTime = DateTime.Now;
            process.UpdateUserId = CurrentUser.UserId;
            process.UpdateUserName = CurrentUser.Name;
            return Json(await _workflowProcessLogic.SaveDesignJson(process));
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [CreateBy("孙泽伟")]
        [Remark("流程维护-方法-删除流程信息", RemarkFrom.Workflow)]
        [Route("/workflow/process/delete")]
        public async Task<JsonResult> Delete(IdInput<string> input)
        {
            return Json(await _workflowProcessLogic.Delete(input));
        }

        /// <summary>
        /// 根据id获取
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Remark("流程维护-方法-根据id获取", RemarkFrom.Workflow)]
        [Route("/workflow/process/{id}")]
        public async Task<JsonResult> FindById([FromRoute] IdInput input)
        {
            return Json(await _workflowProcessLogic.FindById(input));
        }

        /// <summary>
        /// 根据id获取获取预览数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CreateBy("孙泽伟")]
        [Remark("流程维护-方法-根据id获取获取预览数据", RemarkFrom.Workflow)]
        [Route("/workflow/process/preview/{id}")]
        public async Task<JsonResult> FindPreviewById([FromRoute] IdInput input)
        {
            return Json(await _workflowProcessLogic.FindById(input));
        }
        #endregion
    }
}